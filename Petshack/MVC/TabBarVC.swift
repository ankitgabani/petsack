//
//  TabBarVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit

class TabBarVC: UITabBarController {
    
    //MARK: - view cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
     //   self.tabBarController?.tabBar.isTranslucent = false
        
        UITabBar.appearance().unselectedItemTintColor = UIColor.black


        // Do any additional setup after loading the view.
    }
}
