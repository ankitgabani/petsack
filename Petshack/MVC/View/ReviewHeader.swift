//
//  ReviewHeader.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 28/10/21.
//

import UIKit
import HCSStarRatingView

class ReviewHeader: UIView {
    
    
    @IBOutlet weak var lblTotalCustomer: UILabel!
    
    @IBOutlet weak var ratingView: HCSStarRatingView!
    
    @IBOutlet weak var lblTotalReview: UILabel!
    
}
