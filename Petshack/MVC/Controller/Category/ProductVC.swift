//
//  ProductVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 13/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class ProductVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //MARK: - Collection view flowOut
    @IBOutlet weak var igmPRo: UIImageView!
    
    let sectionInsets = UIEdgeInsets(top: 0,
                                     left: 0,
                                     bottom: 0,
                                     right: 0)
    let itemsPerRow: CGFloat = 2
    
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 200)
        
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        // edit properties here
        
        return _flowLayout
    }
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var collView: UICollectionView!
    
    //MARK: - variables
    var strID =  String()
    var strSubCategoryID = String()
    
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()
    var arrProductList : [ProductListModelProduct] = [ProductListModelProduct]()
    
    var isFromSideMenu = false
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        igmPRo.isHidden = true
        collView.delegate = self
        collView.dataSource = self
        self.collView.collectionViewLayout = flowLayout
        
        lblCartCount.layer.cornerRadius = lblCartCount.frame.height/2
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height/2
        
        callingProductDataAPI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.callingOrderListAPI()

        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    //MARK: - collection view delegate and datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProductList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath)as! ProductCollectionCell
        
        let dicData = arrProductList[indexPath.row]
                
        let name = dicData.productName ?? ""
        cell.lblName.text = name 
        
        let images = dicData.img ?? ""
        let placeHolderImage = UIImage(named: "logo")
        if let img = images as? String{
            let image = "\(image_path_product)\(img)"
            
            let url = URL(string: image)
            cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            cell.imgProduct.image = UIImage(named: "logo")
        }
        
        let price = dicData.price ?? ""
        let discount_price = dicData.discountPrice ?? ""
        
        let strPrice = Float(price)
        let discount_amount1 = Float(discount_price)
        
        let subtraction = "\(strPrice! - discount_amount1!)"
        let sub_value = Float(subtraction)
        cell.lblOldPrice.attributedText = "₹ \(price)".strikeThrough()
        cell.lblPrice.text = "₹ \(subtraction)"
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "ProductDetailVC")as! ProductDetailVC
        let arrDeatil = self.arrProductList[indexPath.row]
        vc.dicProductList = arrDeatil
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //MARK: - IBAction
    @IBAction func clickedBack(_ sender: Any) {
        
        if isFromSideMenu == true{
                    appDelegate.setUpSideMenu()
                }else{
                    self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func clickedLike(_ sender: Any) {
        
       
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "FavoriteVC")as! FavoriteVC
                    self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedNotify(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        
       
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "MyCartVC")as! MyCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    //MARK: - API Calling
    
    func callingProductDataAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let param = ["search" : "","cat" : strID,"sub_cat" : strSubCategoryID,"order" : "A"]
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(PRODUCTS, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {

                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        for obj in arrResult {
                            let dicData = ProductListModelProduct(fromDictionary: (obj as? NSDictionary)!)
                            
                            let cat_Id = dicData.subCatId ?? ""
                            
                            if cat_Id == self.strSubCategoryID
                            {
                                self.arrProductList.append(dicData)
                            }
                        }
                        
                        if self.arrProductList.count == 0
                        {
                            self.igmPRo.isHidden = false
                        }
                        else
                        {
                            self.igmPRo.isHidden = true

                        }
                        self.collView.reloadData()
                        
                        DispatchQueue.main.async {
                            GIFHUD.shared.dismiss()
                        }
                        
                    }else{
                        GIFHUD.shared.dismiss()
                    }
                    
                }
                GIFHUD.shared.dismiss()
                
            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }

    func callingOrderListAPI() {
        
      
        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                            
                           if self.arrOrderList.count > 0{
                                let arrCount = self.arrOrderList.count
                                self.lblCartCount.text = "\(arrCount)"
                            }
                        }
                        
                        if arrResult.count == 0
                        {
                            self.lblCartCount.text = "0"
                        }
                        
                    }else{
                    }
                }

            }
            else
            {
            }
        }
    }
    
}
