//
//  CategoriesCollectionCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 13/09/21.
//

import UIKit

class CategoriesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
