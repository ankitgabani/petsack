//
//  ReviewCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 28/10/21.
//

import UIKit
import HCSStarRatingView

class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var viewReview: HCSStarRatingView!
       @IBOutlet weak var lblName: UILabel!
       @IBOutlet weak var lblDate: UILabel!
       @IBOutlet weak var lblReview: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
