//
//  ProductDetailVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 13/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class ProductDetailVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource  {
    
    //MARK: - Collection view flowOut
    
    let sectionInsets = UIEdgeInsets(top: 0,
                                     left: 0,
                                     bottom: 0,
                                     right: 0)
    let itemsPerRow: CGFloat = 2
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 200)
        
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        // edit properties here
        
        return _flowLayout
    }
    
    var flowLayout1: UICollectionViewFlowLayout {
        let flowLayout1 = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        flowLayout1.itemSize = CGSize(width: 120, height: 45)
        
        
        flowLayout1.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        flowLayout1.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flowLayout1.minimumInteritemSpacing = 0.0
        flowLayout1.minimumLineSpacing = 0
        // edit properties here
        
        return flowLayout1
    }
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var collViewSize: UICollectionView!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var txtDescription: UITextView!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnAddCart: UIButton!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var viewOrder: UIView!
    
    var dicProductList : ProductListModelProduct?
    var arrOfferProduct : GetHomeDataFeaturedProduct?
    var arrLatestProduct : GetHomeDataFeaturedProduct?
    var arrFeaturedProduct : GetHomeDataFeaturedProduct?
    
    var isFromOffer = false
    var isFromLatest = false
    var isFromFeatured = false
    var strRealPrice = String()
    
    var isFromSearch = false
    var dicResult : Product?
    
    var arrReview : [ProductDetailModelReview] = [ProductDetailModelReview]()
    var arrProduct : [ProductDetailModelProduct] = [ProductDetailModelProduct]()
    var arrOption : [ProductDetailModelOption] = [ProductDetailModelOption]()
    var arrRelated : [ProductDetailModelRelated] = [ProductDetailModelRelated]()
    var arrProduct_pic : [ProductDetailModelProductPic] = [ProductDetailModelProductPic]()
    
    var dicRelatedProducts : ProductDetailModelRelated?
    
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()
    
    var pid = String()
    var strPrice = String()
    var strName = String()
    var strImg = String()
    var strDesc = String()
    var strDiscount = String()
    
    var isRelatedProduct = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        self.tblView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 10,right: 0)
        
        collView.delegate = self
        collView.dataSource = self
        collViewSize.delegate = self
        collViewSize.dataSource = self
        self.collView.collectionViewLayout = flowLayout
        self.collViewSize.collectionViewLayout = flowLayout1
        self.setUpData()
        self.viewCart.isHidden = true
        self.viewOrder.isHidden = false
        
        lblCartCount.layer.cornerRadius = lblCartCount.frame.height/2
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height/2
        callingProductDetailAPI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.callingOrderListAPI()
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    
    //MARK: - set Data
    func setUpData(){
        
        if isFromSearch == true{
            
            self.pid = dicResult!.id ?? ""
            
            self.strName = dicResult!.productName ?? ""
            
            self.strDiscount = dicResult!.discountPrice ?? ""
            
            self.strImg = dicResult!.img ?? ""
            
            self.strDesc = dicResult!.descriptionField ?? ""
            
            self.strPrice = dicResult?.price ?? ""
            let discount_price = dicResult?.discountPrice ?? ""
            
            let strPrice = Float(self.strPrice)
            let discount_amount1 = Float(discount_price)
            
            self.strRealPrice = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(self.strRealPrice)
            
        }else if isFromLatest{
            
            self.pid = arrLatestProduct!.id ?? ""
            
            self.strName = arrLatestProduct?.productName ?? ""
            
            self.strDiscount = arrLatestProduct!.discountPrice ?? ""
            
            
            self.strImg = arrLatestProduct!.img ?? ""
            
            self.strDesc = arrLatestProduct!.descriptionField ?? ""
            
            self.strPrice = arrLatestProduct?.price ?? ""
            let discount_price = arrLatestProduct?.discountPrice ?? ""
            
            let strPrice = Float(self.strPrice)
            let discount_amount1 = Float(discount_price)
            
            self.strRealPrice = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(self.strRealPrice)
            
        }else if isFromFeatured{
            
            self.pid = arrFeaturedProduct!.id ?? ""
            
            self.strName = arrFeaturedProduct?.productName ?? ""
            
            self.strDiscount = arrFeaturedProduct!.discountPrice ?? ""
            
            self.strImg = arrFeaturedProduct!.img ?? ""
            
            self.strDesc = arrFeaturedProduct!.descriptionField ?? ""
            
            self.strPrice = arrFeaturedProduct?.price ?? ""
            let discount_price = arrFeaturedProduct?.discountPrice ?? ""
            
            let strPrice = Float(self.strPrice)
            let discount_amount1 = Float(discount_price)
            
            self.strRealPrice = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(self.strRealPrice)
            
        }else if isFromOffer{
            
            let response = arrOfferProduct
            
            self.pid = arrOfferProduct!.id ?? ""
            
            self.strName = arrOfferProduct?.productName ?? ""
            
            self.strDiscount = arrOfferProduct!.discountPrice ?? ""
            
            self.strImg = arrOfferProduct!.img ?? ""
            
            self.strDesc = arrOfferProduct!.descriptionField ?? ""
            
            self.strPrice = arrOfferProduct?.price ?? ""
            let discount_price = arrOfferProduct?.discountPrice ?? ""
            
            let strPrice = Float(self.strPrice)
            let discount_amount1 = Float(discount_price)
            
            self.strRealPrice = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(self.strRealPrice)
            
        }else if isRelatedProduct {
            
            self.pid = dicRelatedProducts!.id ?? ""
            
            self.strName = dicRelatedProducts?.productName ?? ""
            
            self.strDiscount = dicRelatedProducts!.discountPrice ?? ""
            
            self.strImg = dicRelatedProducts!.img ?? ""
            
            self.strDesc = dicRelatedProducts!.descriptionField ?? ""
                        
            self.strPrice = dicRelatedProducts?.price ?? ""
            let discount_price = dicRelatedProducts?.discountPrice ?? ""
            
            let strPrice = Float(self.strPrice)
            let discount_amount1 = Float(discount_price)
            
            self.strRealPrice = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(self.strRealPrice)
            
            
        }
        else{
            
            self.pid = dicProductList!.id ?? ""
            
            self.strName = dicProductList!.productName ?? ""
            
            self.strDiscount = dicProductList!.discountPrice ?? ""
            
            self.strImg = dicProductList!.img ?? ""
            
            
            let price = dicProductList?.price ?? ""
            self.strPrice = price

            let discount_price = dicProductList?.discountPrice ?? ""
            self.strDiscount = discount_price
            
            let strPrice = Float(price)
            let discount_amount1 = Float(discount_price)
            
            let subtraction = "\(strPrice! - discount_amount1!)"
            
            
            self.strRealPrice = subtraction

            let sub_value = Float(subtraction)
            self.lblPrice.text = "₹ \(subtraction)"
            
            self.strDesc = dicProductList!.descriptionField ?? ""
        }
        
        self.lblName.text = strName
        
        let placeHolderImage = UIImage(named: "logo")
        if let img = self.strImg as? String{
            let image = "\(image_path_product)\(img)"
            
            let url = URL(string: image)
            self.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            self.imgPhoto.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            self.imgProduct.image = UIImage(named: "logo")
            self.imgPhoto.image = UIImage(named: "logo")
        }
        
        self.txtDescription.text = "\(self.strDesc)"
        
    }
    
    //MARK: - tableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReview.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("ReviewHeader", owner: self, options: nil)![0] as? ReviewHeader
        
        let count = self.arrReview.count ?? 0
        headerView?.lblTotalCustomer.text = "\(count) customer"
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
        
        let arrCustomerReview = arrReview[indexPath.row]
        
        let name = arrCustomerReview.name ?? ""
        cell.lblName.text = name
        
        
        let addon = arrCustomerReview.addon ?? ""
        
        var deliverycureentDateString = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let arrSplit = addon.components(separatedBy: " ")
        let objct1 = arrSplit[0] as! String
        let objct2 = arrSplit[1] as! String
        
        let arrSplit12 = objct2.components(separatedBy: ":")
        let strYear2 = arrSplit12[0] as! String
        let strMonth2 = arrSplit12[1] as! String
        
        
        let arrSplit1 = objct1.components(separatedBy: "-")
        let strYear = arrSplit1[0] as! String
        let strMonth = arrSplit1[1] as! String
        let strDay = arrSplit1[2] as! String
        
        let monthNumber = (Int)(strMonth)
        let fmt = DateFormatter()
        fmt.dateStyle = .medium
        let strMonthName = fmt.monthSymbols[monthNumber! - 1]
        deliverycureentDateString = String.init(format: "%@ %@ %@, %@:%@", strMonthName.prefix(3) as CVarArg,strDay,strYear,strYear2,strMonth2)
        
        cell.lblDate.text = deliverycureentDateString
        
        
        let review = arrCustomerReview.rating ?? ""
        let toFloat = CGFloat((review as NSString).floatValue)
        cell.lblReview.text = arrCustomerReview.review ?? ""
        
        cell.viewReview.value = toFloat
        
        return cell
    }
    
    //MARK: - collection view delegate and datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collViewSize{
            return arrOption.count
        }else{
            return arrRelated.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collViewSize{
            
            let cell = collViewSize.dequeueReusableCell(withReuseIdentifier: "ProductSizeCell", for: indexPath)as! ProductSizeCell
            
            let arrSize = self.arrOption[indexPath.row]
            
            let name = arrSize.title ?? ""
            cell.lblSize.text = name
            
//            let option =  self.arrOption[0]
//
//            let price = option.value ?? ""
//            self.lblPrice.text = "₹ \(price)"
            
            return cell
            
        }else{
            let cell = collView.dequeueReusableCell(withReuseIdentifier: "RelatedProductCell", for: indexPath)as! RelatedProductCell
            
            let dicData = arrRelated[indexPath.row]
            
            let name = dicData.productName ?? ""
            cell.lblName.text = name
            
            let images = dicData.img ?? ""
            let placeHolderImage = UIImage(named: "logo")
            if let img = images as? String{
                let image = "\(image_path_product)\(img)"
                
                let url = URL(string: image)
                cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgProduct.image = UIImage(named: "logo")
            }
            
            let price = dicData.price ?? ""
            let discount_price = dicData.discountPrice ?? ""
            
            let qty = dicData
            
            let strPrice = Float(price)
            let discount_amount1 = Float(discount_price)
            
            let subtraction = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(subtraction)
            cell.lblOldPrice.attributedText = "₹ \(price)".strikeThrough()
            cell.lblPrice.text = "₹ \(subtraction)"
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collViewSize{
            
//            let arrData = self.arrOption[indexPath.row]
//            self.strPrice = arrData.value ?? ""
//            self.lblPrice.text = "₹ \(self.strPrice)"
            
        }else{
            
            let vc = self.storyboard?.instantiateViewController(identifier: "ProductDetailVC")as! ProductDetailVC
            let arrDeatil = self.arrRelated[indexPath.row]
            vc.dicRelatedProducts = arrDeatil
            vc.isRelatedProduct = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedLike(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "FavoriteVC")as! FavoriteVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedNotify(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        
      
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "MyCartVC")as! MyCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedAddCart(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            self.viewCart.isHidden = false
            self.viewOrder.isHidden = true
            callingAddCartAPI()
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
      
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        
        let totalquantity = Int(self.lblCount.text!)
        
        if totalquantity! > 0 {
            let newValue = totalquantity! + 1
            
            self.lblCount.text = "\(newValue)"
            
            callingUpdateCartAPI(id : pid,qty: self.lblCount.text!)
        }
        
    }
    
    @IBAction func btnRemove(_ sender: Any) {
        
        let totalquantity = Int(self.lblCount.text!)
        
        if totalquantity! > 0 {
            let newValue = totalquantity! - 1
            
            self.lblCount.text = "\(newValue)"
            
            callingUpdateCartAPI(id: pid, qty: self.lblCount.text!)
            
            if newValue > 0{
                self.viewCart.isHidden = false
                self.viewOrder.isHidden = true
            }else{
                self.viewCart.isHidden = true
                self.viewOrder.isHidden = false
            }
        }
        
    }
    
    //MARK: - API Calling
    
    func callingOrderListAPI() {
        
        
        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                            
                            if self.arrOrderList.count > 0{
                                let arrCount = self.arrOrderList.count
                                self.lblCartCount.text = "\(arrCount)"
                            }
                        }
                        
                        if arrResult.count == 0
                        {
                            self.lblCartCount.text = "0"
                        }
                        
                    }else{
                    }
                }
                
            }
            else
            {
            }
        }
    }
    
    func callingAddCartAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 1.5)
        
        let dicData = appDelegate.dicLoginUserDetails

        let user_id = dicData.id
        
        
        if isFromSearch == true{
            strPrice = dicResult?.price ?? ""
        }
        
        let param = ["user_id" : user_id ?? "","real_price": self.strRealPrice , "id" : pid ,"name" : strName,"price": strPrice,"discount" : strDiscount,"img" : strImg] as [String : Any]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(ADD_CART, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            
            if error == nil
            {
                let status = response!.value(forKey: "status")as? Bool
                let msg = response!.value(forKey: "result")as? String

                if statusCode == 200{
                    GIFHUD.shared.dismiss()
                    if status == true{
                        
                        self.view.makeToast(msg)
                        self.callingOrderListAPI()
                    }
                }
                else
                {
                    self.view.makeToast(msg)
                }
                GIFHUD.shared.dismiss()
            }
            else
            {

                GIFHUD.shared.dismiss()
            }
        }
    }
    
    func callingUpdateCartAPI(id : String,qty: String) {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 1.5)
        
        
        let dicData = appDelegate.dicLoginUserDetails

        let email = dicData.email ?? ""
        let uid = dicData.id ?? ""
        
        let param = ["user_id" : uid,"id" : id,"qty" : qty]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(UPDATE_CART, parameters: param) { (response, error, statusCode) in
            
            
            if error == nil
            {
                let status = response!.value(forKey: "status")as! Bool
                let msg = response!.value(forKey: "result")as! String

                if statusCode == 200{
                    
                    
                    if status == true {
                        DispatchQueue.main.async {
                            self.view.makeToast(msg)
                            
                            self.callingOrderListAPI()
                        }
                    }else{
                        self.view.makeToast(msg)
                        self.callingOrderListAPI()
                    }
                }
                else
                {
                    self.view.makeToast(msg)
                }
                GIFHUD.shared.dismiss()
            }else{
                let msg = response!.value(forKey: "result")as! String

                self.view.makeToast(msg)
                GIFHUD.shared.dismiss()
            }
        }
    }
    
    func callingProductDetailAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 3.0)
        
        let id = self.pid
        let form_data = "?id="
        let URL = "\(PRODUCT_DETAIL)\(form_data)\(id)"
        let param = ["" : ""]
        
        print(param)
        print(URL)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    GIFHUD.shared.dismiss()
                    GIFHUD.shared.dismiss()

                    let status = response!.value(forKey: "status")as! Bool
                    
                    if status == true{
                        
                        let arrReview = response!.value(forKey: "review") as? NSArray
                        let arrProduct = response!.value(forKey: "product") as? NSArray
                        let arrOption = response!.value(forKey: "option") as? NSArray
                        let arrRelated = response!.value(forKey: "related") as? NSArray
                        let arrProduct_pic = response!.value(forKey: "product_pic") as? NSArray
                        
                        for obj in arrReview! {
                            let dicData = ProductDetailModelReview(fromDictionary: (obj as? NSDictionary)!)
                            self.arrReview.append(dicData)
                        }
                        for obj in arrProduct! {
                            let dicData = ProductDetailModelProduct(fromDictionary: (obj as? NSDictionary)!)
                            self.lblPrice.text = "₹ \(dicData.price ?? "")"
                            self.arrProduct.append(dicData)
                        }
                        if (arrOption?.count ?? 0) > 0
                        {
                            for obj in arrOption! {
                                let dicData = ProductDetailModelOption(fromDictionary: (obj as? NSDictionary)!)
                                self.arrOption.append(dicData)
                            }
                        }
                        
                        if (arrRelated?.count ?? 0) > 0
                        {
                            for obj in arrRelated! {
                                let dicData = ProductDetailModelRelated(fromDictionary: (obj as? NSDictionary)!)
                                self.arrRelated.append(dicData)
                            }
                        }
                        
                        if (arrProduct_pic?.count ?? 0) > 0
                        {
                            for obj in arrProduct_pic! {
                                let dicData = ProductDetailModelProductPic(fromDictionary: (obj as? NSDictionary)!)
                                self.arrProduct_pic.append(dicData)
                            }
                        }

                        self.collViewSize.reloadData()
                        self.collView.reloadData()
                        self.tblView.reloadData()
                    }
                }
                else
                {
                    GIFHUD.shared.dismiss()
                }
                
            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
    
    
}
