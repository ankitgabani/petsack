//
//  ProductCollectionCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 13/09/21.
//

import UIKit

class ProductCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTypes: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
}
