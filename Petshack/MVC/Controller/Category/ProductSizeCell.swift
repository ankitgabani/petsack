//
//  ProductSizeCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 22/10/21.
//

import UIKit

class ProductSizeCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblSize: UILabel!
    
}
