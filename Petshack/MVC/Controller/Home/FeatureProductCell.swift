//
//  FeatureProductCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 22/09/21.
//

import UIKit

class FeatureProductCell: UICollectionViewCell {
    
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTypes: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
}
