//
//  MyOrderVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class MyOrderVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrOrders : [OrderListTabOrder] =  [OrderListTabOrder]()
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()
    
    //MARK: - Variables
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shadowView()
        tblView.delegate = self
        tblView.dataSource = self
        tblView.register(UINib(nibName: "MyOrderCell", bundle: nil), forCellReuseIdentifier: "MyOrderCell")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {

        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            self.tabBarController?.tabBar.isHidden = false
            callingGetOrderAPI()
            callingOrderListAPI()
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isFromSide = true
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    //MARK: - tableview delegate-datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MyOrderCell", for: indexPath) as! MyOrderCell
        
        cell.selectionStyle = .none
        
        let dicData = self.arrOrders[indexPath.row]
        let id = dicData.orderId ?? ""
        cell.lblOrderNo.text = id
        
        let price = dicData.total ?? ""
        cell.lblPrice.text = "₹ \(price)"
        
        let mode = dicData.paymentMode ?? ""
        cell.lblMode.text = "Mode: \(mode)"
        
        let date = dicData.created ?? ""
        cell.lblDate.text = date
        
        let cancel_pro = dicData.cancelPro ?? ""
        if cancel_pro == "0"{
            cell.btnPlaced.setTitle("Placed", for: .normal)
        }else{
            cell.btnPlaced.setTitle("Cancelled", for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "OrderDetailVC")as! OrderDetailVC
        let arrOrder = self.arrOrders[indexPath.row]
        vc.arrOrderDetail = arrOrder
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - user function
    
    func shadowView(){
        
        lblCartCount.layer.cornerRadius = lblCartCount.frame.height/2
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height/2
        
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedMenu(_ sender: Any) {
        
        self.sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    
    @IBAction func clickedLike(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "FavoriteVC")as! FavoriteVC
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedNotify(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "MyCartVC")as! MyCartVC
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
    }
    
    
    func callingGetOrderAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let dicData = appDelegate.dicLoginUserDetails
        
        let ID = dicData.id
        
        let id = ID ?? ""
        let form_data = "?user_id="
        let URL = "\(ORDER_LIST_TAB)\(form_data)\(id)"
        let param = ["" : ""]
        
        print(param)
        print(URL)
        
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as? Bool
                    let dicData = response!.value(forKey: "data")as? NSDictionary
                    
                    if status == true{
                        
                        self.arrOrders.removeAll()
                        
                        if dicData != nil
                        {
                            
                            let arrResult = dicData?.value(forKey: "order")as? NSArray
                            
                            for obj in arrResult! {
                                let dicData = OrderListTabOrder(fromDictionary: (obj as? NSDictionary)!)
                                self.arrOrders.append(dicData)
                            }
                            
                        }
                        
                        self.tblView.reloadData()
                    }
                }
                else
                {
                    self.arrOrders.removeAll()
                    self.tblView.reloadData()
                }
                GIFHUD.shared.dismiss()
            }
            else
            {
                GIFHUD.shared.dismiss()
                self.arrOrders.removeAll()
                self.tblView.reloadData()
            }
        }
    }
    
    func callingOrderListAPI() {
        
        let dicData = appDelegate.dicLoginUserDetails
        
        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                            
                            if self.arrOrderList.count > 0{
                                let arrCount = self.arrOrderList.count
                                self.lblCartCount.text = "\(arrCount)"
                            }
                        }
                        
                        if arrResult.count == 0
                        {
                            self.lblCartCount.text = "0"
                        }
                        
                    }else{
                    }
                }
                
            }
            else
            {
            }
        }
    }
    
}
