//
//  HomeVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit
import LGSideMenuController
import SDWebImage
import GiFHUD_Swift
import SVProgressHUD

class HomeVC: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    //MARK: - IBAction
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var collViewFeatureProducts: UICollectionView!
    @IBOutlet weak var sliderCollView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imgBanner1: UIImageView!
    @IBOutlet weak var imgBanner2: UIImageView!
    @IBOutlet weak var imgBanner3: UIImageView!
    @IBOutlet weak var collViewLatestProduct: UICollectionView!
    @IBOutlet weak var collViewOfferProduct: UICollectionView!
    //MARK: - Collection flowOut
    
    var flowLayout1: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 85, height: 110)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0
        _flowLayout.minimumLineSpacing = 0
        // edit properties here
        
        return _flowLayout
    }
    
    let sectionInsets = UIEdgeInsets(top: 12.0,
                                     left: 12.0,
                                     bottom: 12.0,
                                     right: 12.0)
    let itemsPerRow: CGFloat = 1
    
    
    var flowLayoutAuto: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: self.sliderCollView.frame.width, height: self.sliderCollView.frame.height)
        }
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        return _flowLayout
    }
    
    
    let sectionInsets1 = UIEdgeInsets(top: 0,
                                      left: 0,
                                      bottom: 0,
                                      right: 0)
    let itemsPerRow1: CGFloat = 2
    
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets1.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow1
        
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 200)
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        // edit properties here
        
        return _flowLayout
    }
    
    //MARK: - Variables
    
    //Banner
    var arrHomeBanner1 : [GetHomeDataHomeLeft] = [GetHomeDataHomeLeft]()
    var arrHomeBanner2 : [GetHomeDataHomeLeft] = [GetHomeDataHomeLeft]()
    var arrHomeBanner3 : [GetHomeDataHomeLeft] = [GetHomeDataHomeLeft]()
    
    //category
    var arrCategory = NSArray()
    var arrDetail : [GetHomeDataBread] = [GetHomeDataBread]()
    var arrSubCategory : [GetHomeDataSubCategory] = [GetHomeDataSubCategory]()
    var strProductId = Int()
    
    //products
    var arrOfferProduct : [GetHomeDataFeaturedProduct] = [GetHomeDataFeaturedProduct]()
    var arrLatestProduct : [GetHomeDataFeaturedProduct] = [GetHomeDataFeaturedProduct]()
    var arrFeaturedProduct : [GetHomeDataFeaturedProduct] = [GetHomeDataFeaturedProduct]()
    
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()

    // : [GetHomeDataBread] = [GetHomeDataBread]()
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //       startTimer()
        self.shadowView()
        
        self.collView.collectionViewLayout = flowLayout1
        self.sliderCollView.collectionViewLayout = flowLayoutAuto
        self.collViewFeatureProducts.collectionViewLayout = flowLayout
        self.collViewLatestProduct.collectionViewLayout = flowLayout
        self.collViewOfferProduct.collectionViewLayout = flowLayout
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.callingHomeDataAPI()

        self.callingOrderListAPI()
    }
    
    
    //MARK: - user function
    
    func shadowView(){
        
        lblCartCount.layer.cornerRadius = lblCartCount.frame.height/2
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height/2
        
    }
    
    //MARK: - Collection view delegate & datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collView{
            return self.arrDetail.count
        }else if collectionView == sliderCollView{
            return 1
        }else if collectionView == collViewFeatureProducts{
            return arrFeaturedProduct.count
        }else if collectionView == collViewLatestProduct{
            return arrLatestProduct.count
        }else{
            return arrOfferProduct.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collView{
            let cell = collView.dequeueReusableCell(withReuseIdentifier: "AnimalCategoryCell", for: indexPath)as! AnimalCategoryCell
            
            cell.viewBackground.layer.cornerRadius = cell.viewBackground.frame.height/2
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.height/2
            
            //MARK: - banner1
            
            let arrBanner = arrHomeBanner1[0]
            let image = arrBanner.img ?? ""
            let placeHolderImage = UIImage(named: "logo")
            
            if let img = image as? String{
                let image = "\(image_path_banner)\(img)"
                let url = URL(string: image)
                self.imgBanner1.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                self.imgBanner1.image = UIImage(named: "logo")
            }
            
            //MARK: - banner2
            
            let arrBannerMiddle = arrHomeBanner2[0]
            let image1 = arrBannerMiddle.img ?? ""
            
            if let img = image1 as? String{
                let image = "\(image_path_banner)\(img)"
                let url = URL(string: image)
                self.imgBanner2.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                self.imgBanner2.image = UIImage(named: "logo")
            }
            
            //MARK: - banner3
            
            let arrBannerBottom = arrHomeBanner3[0]
            let image2 = arrBannerBottom.img ?? ""
            
            if let img = image2 as? String{
                let image = "\(image_path_banner)\(img)"
                let url = URL(string: image)
                self.imgBanner3.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                self.imgBanner3.image = UIImage(named: "logo")
            }
            
            //MARK: - Category
            
            let arrayDetail = arrDetail[indexPath.row]
            self.arrSubCategory = arrayDetail.subCategory
            
            let name = arrayDetail.name ?? ""
            cell.lblName.text = name
            
            let images = arrayDetail.catPic ?? ""
            if let img = images as? String{
                let image = "\(image_path)\(img)"
                
                let url = URL(string: image)
                cell.imgCategory.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgCategory.image = UIImage(named: "logo")
            }
            
            
            return cell
            
        }else if collectionView == sliderCollView{
            
            let cell = sliderCollView.dequeueReusableCell(withReuseIdentifier: "SliderCell", for: indexPath)as! SliderCell
            
            //     self.pageControl.numberOfPages = 5
            
            return cell
            
        }else if collectionView == collViewFeatureProducts{
            
            let cell = collViewFeatureProducts.dequeueReusableCell(withReuseIdentifier: "FeatureProductCell", for: indexPath)as! FeatureProductCell
            
            let arrFeatured = self.arrFeaturedProduct[indexPath.row]
            
            let name = arrFeatured.productName ?? ""
            cell.lblName.text = name.maxLength(length: 30)
            
            let placeHolderImage = UIImage(named: "logo")
            let images = arrFeatured.img ?? ""
            if let img = images as? String{
                let image = "\(image_path_product)\(img)"
                
                let url = URL(string: image)
                cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgProduct.image = UIImage(named: "logo")
            }
            
            let price = arrFeatured.price ?? ""
            let discount_price = arrFeatured.discountPrice ?? ""
            
            let strPrice = Float(price)
            let discount_amount1 = Float(discount_price)
            
            let subtraction = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(subtraction)
            
            cell.lblOldPrice.attributedText = "₹ \(price)".strikeThrough()
            cell.lblPrice.text = "₹ \(subtraction)"

            return cell
            
        }else if collectionView == collViewLatestProduct{
            
            let cell = collViewLatestProduct.dequeueReusableCell(withReuseIdentifier: "LatestProductCell", for: indexPath)as! LatestProductCell
            
            let arrLatest = self.arrLatestProduct[indexPath.row]
            
            let name = arrLatest.productName ?? ""
            cell.lblName.text = name.maxLength(length: 30)
            
            let placeHolderImage = UIImage(named: "logo")
            let images = arrLatest.img ?? ""
            if let img = images as? String{
                let image = "\(image_path_product)\(img)"
                
                let url = URL(string: image)
                cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgProduct.image = UIImage(named: "logo")
            }
            
            let price = arrLatest.price ?? ""
            let discount_price = arrLatest.discountPrice ?? ""
            
            let strPrice = Float(price)
            let discount_amount1 = Float(discount_price)
            
            let subtraction = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(subtraction)
            cell.lblOldPrice.attributedText = "₹ \(price)".strikeThrough()
            cell.lblPrice.text = "₹ \(subtraction)"

            
       
            return cell
            
        }else {
            let cell = collViewOfferProduct.dequeueReusableCell(withReuseIdentifier: "OfferProductCell", for: indexPath)as! OfferProductCell
            
            let arrOffer = self.arrOfferProduct[indexPath.row]
            
            let name = arrOffer.productName ?? ""
            cell.lblName.text = name.maxLength(length: 30)
            
            let placeHolderImage = UIImage(named: "logo")
            let images = arrOffer.img ?? ""
            if let img = images as? String{
                let image = "\(image_path_product)\(img)"
                
                let url = URL(string: image)
                cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgProduct.image = UIImage(named: "logo")
            }
            
            let price = arrOffer.price ?? ""
            let discount_price = arrOffer.discountPrice ?? ""
            
            let strPrice = Float(price)
            let discount_amount1 = Float(discount_price)
            
            let subtraction = "\(strPrice! - discount_amount1!)"
            let sub_value = Float(subtraction)
            cell.lblOldPrice.attributedText = "₹ \(price)".strikeThrough()
            cell.lblPrice.text = "₹ \(subtraction)"

            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collView{
            let vc = self.storyboard?.instantiateViewController(identifier: "CategoryVC")as! CategoryVC
            let arrDetails = self.arrDetail[indexPath.row]
            let sub_category =  arrDetails.subCategory
            vc.strName = arrDetails.name
            vc.arrSubCategory = sub_category!
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView ==  sliderCollView{
            
        }else if collectionView == collViewFeatureProducts{
            let vc = self.storyboard?.instantiateViewController(identifier: "ProductDetailVC")as! ProductDetailVC
            let arrFeatured = self.arrFeaturedProduct[indexPath.row]
            vc.isFromFeatured = true
            vc.arrFeaturedProduct = arrFeatured
//            vc.strSubCategoryID = arrFeatured.subCatId
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView == collViewLatestProduct{
            let vc = self.storyboard?.instantiateViewController(identifier: "ProductDetailVC")as! ProductDetailVC
            let arrLatest = self.arrLatestProduct[indexPath.row]
            vc.isFromLatest = true
            vc.arrLatestProduct = arrLatest
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(identifier: "ProductDetailVC")as! ProductDetailVC
            let arrOffer = self.arrOfferProduct[indexPath.row]
            vc.isFromOffer = true
            vc.arrOfferProduct = arrOffer
//            let arrOffer = self.arrOfferProduct[indexPath.row]
//            vc.strSubCategoryID = arrOffer.subCatId
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    //MARK: - Timer
    //    func startTimer() {
    //
    //        let timer =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
    //    }
    //
    
    //MARK: - Scroll to Next Cell
    
    //    @objc func scrollToNextCell(){
    //
    //        if pageControl.currentPage == pageControl.numberOfPages - 1 {
    //            pageControl.currentPage = 0
    //        } else {
    //            pageControl.currentPage += 1
    //        }
    //
    //      //  sliderCollView.scrollToItem(at: IndexPath(row: pageControl.currentPage, section: 0), at: .right, animated: true)
    //    }
    
    
    //MARK: - IBAction
    
    @IBAction func clickedMenu(_ sender: Any) {
        
        self.sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    
    @IBAction func clickedLike(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "FavoriteVC")as! FavoriteVC
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
        
    }
    
    @IBAction func clickedNotify(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "MyCartVC")as! MyCartVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    //MARK: - API Calling
    
    
    
    func callingHomeDataAPI() {
     
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 3.5)


        //   APIClient.sharedInstance.showIndicator()
        
        let param = ["" : ""]
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(HOME_DATA, parameters: param) { (response, error, statusCode) in
            
            if error == nil
            {
                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                GIFHUD.shared.dismiss()
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSDictionary
                    
                    if status == true{
                        let arrCategory = arrResult.value(forKey: "category")as! NSArray
                        let arrHomeLeft = arrResult.value(forKey: "home_left")as! NSArray
                        let arrHomeRight = arrResult.value(forKey: "home_right")as! NSArray
                        let arrHomeMid = arrResult.value(forKey: "home_mid")as! NSArray
                        let offerProduct = arrResult.value(forKey: "offer_product")as! NSArray
                        let latestProduct = arrResult.value(forKey: "latest_product")as! NSArray
                        let featuredProduct = arrResult.value(forKey: "featured_product")as! NSArray
                        
                        
                        for obj in arrCategory {
                            let dicData = GetHomeDataBread(fromDictionary: (obj as? NSDictionary)!)
                            self.arrDetail.append(dicData)
                        }
                        for obj in arrHomeLeft{
                            let dic = GetHomeDataHomeLeft(fromDictionary: (obj as? NSDictionary)!)
                            self.arrHomeBanner1.append(dic)
                        }
                        for obj in arrHomeRight{
                            let dic = GetHomeDataHomeLeft(fromDictionary: (obj as? NSDictionary)!)
                            self.arrHomeBanner2.append(dic)
                        }
                        for obj in arrHomeMid{
                            let dic = GetHomeDataHomeLeft(fromDictionary: (obj as? NSDictionary)!)
                            self.arrHomeBanner3.append(dic)
                        }
                        for obj in offerProduct{
                            let dic = GetHomeDataFeaturedProduct(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOfferProduct.append(dic)
                        }
                        for obj in latestProduct{
                            let dic = GetHomeDataFeaturedProduct(fromDictionary: (obj as? NSDictionary)!)
                            self.arrLatestProduct.append(dic)
                        }
                        for obj in featuredProduct{
                            let dic = GetHomeDataFeaturedProduct(fromDictionary: (obj as? NSDictionary)!)
                            self.arrFeaturedProduct.append(dic)
                        }
                        
                        
                    }
                  
                    self.collView.reloadData()
                    self.collViewOfferProduct.reloadData()
                    self.collViewLatestProduct.reloadData()
                    self.collViewFeatureProducts.reloadData()
                    GIFHUD.shared.dismiss()
                }
                
            }
            else
            {
                GIFHUD.shared.dismiss()
                self.callingHomeDataAPI()
            }
        }
    }
    
    
    func callingWishlistAPI() {

        let dicData = appDelegate.dicLoginUserDetails

        let email = dicData.email ?? ""
        let uid = dicData.id ?? ""
        
        let param = ["user_id" : uid,"product_id" : ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(WISH_ADD, parameters: param) { (response, error, statusCode) in
            
            
            if error == nil
            {
                let msg = response!.value(forKey: "msg")as! String

                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                
                
                if statusCode == 200{
                    self.view.makeToast(msg)
                }

            }
            else
            {
                let msg = response!.value(forKey: "msg")as! String

                self.view.makeToast(msg)
            }
        }
    }
    
    func callingOrderListAPI() {
        
      
        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                            
                            if self.arrOrderList.count > 0{
                                let arrCount = self.arrOrderList.count
                                self.lblCartCount.text = "\(arrCount)"
                            }
                           
                        }
                        
                        if arrResult.count == 0
                        {
                            self.lblCartCount.text = "0"
                        }
                    }else{
                    }
                }

            }
            else
            {
            }
        }
    }
    
}


extension String {
    func maxLength(length: Int) -> String {
        var str = self
        let nsString = str as NSString
        if nsString.length >= length {
            str = nsString.substring(with:
                                        NSRange(
                                            location: 0,
                                            length: nsString.length > length ? length : nsString.length)
            )
        }
        return  str
    }
}

extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(
            NSAttributedString.Key.strikethroughStyle,
               value: NSUnderlineStyle.single.rawValue,
                   range:NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
