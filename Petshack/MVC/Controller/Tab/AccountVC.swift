//
//  AccountVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit
import GiFHUD_Swift
import Alamofire
import SVProgressHUD
import MobileCoreServices
import Photos

class AccountVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate , UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    //MARK: - Outlets
    
    @IBOutlet weak var consHeightPetProfile: NSLayoutConstraint!
    @IBOutlet weak var consHeight: NSLayoutConstraint!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    @IBOutlet weak var viewAddresses: UIView!
    @IBOutlet weak var btnViewAllAddress: UIButton!
    
    @IBOutlet weak var viewPetProfile: UIView!
    @IBOutlet weak var btnAddPet: UIButton!
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var collViewAddress: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var pageControlAddress: UIPageControl!
    @IBOutlet weak var btnLogout: UIButton!
    
    
    //MARK: - Collectionview flowout
    
    let sectionInsetsSlider = UIEdgeInsets(top: 0.0,
                                           left: 0.0,
                                           bottom: 0.0,
                                           right: 0.0)
    
    var flowLayoutSlider: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsetsSlider.left * (1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 1
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: widthPerItem, height: self.collView.frame.size.height)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _flowLayout.minimumInteritemSpacing = 0.0
            _flowLayout.minimumLineSpacing = 0
        }
        
        return _flowLayout
    }
    
    let sectionInsetsSlider1 = UIEdgeInsets(top: 0.0,
                                            left: 0.0,
                                            bottom: 0.0,
                                            right: 0.0)
    
    var flowLayoutSlider1: UICollectionViewFlowLayout {
        let _flowLayout1 = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsetsSlider1.left * (1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 1
        
        DispatchQueue.main.async {
            _flowLayout1.itemSize = CGSize(width: widthPerItem, height: self.collViewAddress.frame.size.height)
            
            _flowLayout1.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            _flowLayout1.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _flowLayout1.minimumInteritemSpacing = 0.0
            _flowLayout1.minimumLineSpacing = 0
        }
        
        return _flowLayout1
    }
    
    
    
    //MARK: - Variables
    
    var arrPetData : [Result] = [Result]()
    
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()
    
    var imagePicker = UIImagePickerController()
    var objFiledata = String()
    var getFileNameServer = String()
    var selectedImage = UIImage()
    
    
    var arrAddressList : [AddressListModelData] = [AddressListModelData]()
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        collView.delegate = self
        collView.dataSource = self
        collView.collectionViewLayout = flowLayoutSlider
        collViewAddress.collectionViewLayout = flowLayoutSlider1
        
        pageControl.numberOfPages = arrPetData.count
        pageControlAddress.numberOfPages = arrAddressList.count
        
        self.shadowView()
        imagePicker.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            self.tabBarController?.tabBar.isHidden = false

            let dicData = appDelegate.dicLoginUserDetails
            
            let email = dicData.email
            let name = dicData.name
            let mobile = dicData.phone
            self.lblName.text = name
            self.lblEmail.text = email
            self.lblMobile.text = mobile
            
            let objPic = dicData.profile
            
            if ((objPic?.contains("https://petshack.co")) == false)
            {
                let imgPic = "https://petshack.co.in/assets/images/\(dicData.profile ?? "")"
                self.imgProfile.sd_setImage(with: URL.init(string: imgPic), completed: nil)
                
            }
            else
            {
                self.imgProfile.sd_setImage(with: URL.init(string: dicData.profile), completed: nil)
            }
            
            callingGetPetListAPI()
            callingAddressListAPI()
            callingOrderListAPI()
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isFromSide = true
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
    }
    
    //MARK: - Collection view delegate - data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collView{
            return arrPetData.count
        }else{
            return arrAddressList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collView{
            
            let cell = collView.dequeueReusableCell(withReuseIdentifier: "PetProfileCell", for: indexPath)as! PetProfileCell
            
            let arrPetDataDetail = arrPetData[indexPath.row]
            
            let name = arrPetDataDetail.name ?? ""
            let age = " (\(arrPetDataDetail.age ?? "") Years)"
            cell.lblPetName.text = "\(name)\(age)"
            
            let b_name = arrPetDataDetail.breed ?? ""
            cell.lblPetHealth.text = b_name
            
            let about = arrPetDataDetail.healthNote ?? ""
            cell.lblAboutPet.text = about
            
            let placeHolderImage = UIImage(named: "logo")
            let images = arrPetDataDetail.profile ?? ""
            if let img = images as? String{
                let image = "\(image_path_product)\(img)"
                
                let url = URL(string: image)
                cell.imgPetProfile.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgPetProfile.image = UIImage(named: "logo")
            }
            
            cell.btnView.tag = indexPath.row
            cell.btnView.addTarget(self, action: #selector(clickedView(sender:)), for: .touchUpInside)
            
            return cell
            
        }else{
            
            let cell = collViewAddress.dequeueReusableCell(withReuseIdentifier: "AddressDataCell", for: indexPath)as! AddressDataCell
            
            cell.lblAddressType.layer.cornerRadius = 7
            cell.lblAddressType.clipsToBounds = true
            
            let arrAddress = self.arrAddressList[indexPath.row]
            
            let name = arrAddress.fname ?? ""
            cell.lblName.text = name
            
            let street = arrAddress.street ?? ""
            let landmark = arrAddress.landmark ?? ""
            let city = arrAddress.city ?? ""
            let state = arrAddress.state ?? ""
            let country = arrAddress.country ?? ""
            let pincode = arrAddress.pincode ?? ""
            
            cell.lblAddress.text = "\(street),Near\(landmark), \(city),\(state),\(country), \(pincode)"
            
            let phone = arrAddress.phone ?? ""
            cell.lblMobile.text = phone
            
            let type = arrAddress.type ?? ""
            cell.lblAddressType.text = type
            
            return cell
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collViewAddress{
            
        }else{
            
            //            let vc = self.storyboard?.instantiateViewController(identifier: "PetDetailVC")as! PetDetailVC
            //            vc.arrPetData = self.arrPetData[indexPath.row]
            //            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentPage = collView.contentOffset.x / collView.frame.size.width;
        let currentPage1 = collViewAddress.contentOffset.x / collViewAddress.frame.size.width;
        pageControl.currentPage = Int(currentPage)
        pageControl.numberOfPages = arrPetData.count
        pageControlAddress.currentPage = Int(currentPage1)
        pageControlAddress.numberOfPages = arrAddressList.count
    }
    
    //MARK: - button Action
    @objc func clickedView(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "PetDetailVC")as! PetDetailVC
        let objslider = arrPetData[sender.tag]
        vc.arrPetData = objslider
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: - User Function
    
    func shadowView(){
        
        lblCartCount.layer.cornerRadius = lblCartCount.frame.height/2
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height/2
        
        viewAddresses.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        viewAddresses.layer.shadowRadius = 10
        viewAddresses.layer.shadowColor = UIColor.init(red: 08.0/255.0, green: 16.0/255.0, blue: 24.0/255.0, alpha: 1).cgColor
        viewAddresses.layer.shadowOpacity = 0.1
        
        viewPetProfile.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        viewPetProfile.layer.shadowRadius = 10
        viewPetProfile.layer.shadowColor = UIColor.init(red: 08.0/255.0, green: 16.0/255.0, blue: 24.0/255.0, alpha: 1).cgColor
        viewPetProfile.layer.shadowOpacity = 0.1
        
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        btnEdit.layer.cornerRadius = btnEdit.frame.height/2
        
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedMenu(_ sender: Any) {
        
        self.sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    
    @IBAction func clickedLike(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "FavoriteVC")as! FavoriteVC
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
    }
    
    @IBAction func clickedNotify(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: true)
            

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        
       
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "MyCartVC")as! MyCartVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
    }
    
    
    @IBAction func clickedProfile(_ sender: Any) {
        
        choosePicture()
    }
    
    @IBAction func clickedEdit(_ sender: Any) {
    }
    
    @IBAction func clickedViewAddress(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "AddressListVC")as! AddressListVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedPetProfile(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "AddPetProfileVC")as! AddPetProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        
        let alert = UIAlertController(title: "Are you sure want to Logout?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            
            UserDefaults.standard.setValue(false, forKey: "UserLogin")
            UserDefaults.standard.synchronize()
            
            appDelegate.saveCurrentUserData(dic: RegisterDataResult())
            appDelegate.dicLoginUserDetails = RegisterDataResult()
            
            appDelegate.setUpSideMenu()
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: - API Calling:-
    
    func callingAddressListAPI() {
        
        let dicData = appDelegate.dicLoginUserDetails
        
        let ID = dicData.id
        
        let id = ID ?? ""
        let form_data_id = "?user_id="
        let form_data_billing = "&billing=billing"
        let URL = "\(ADDRESS_LIST)\(form_data_id)\(id)\(form_data_billing)"
        
        let param = ["" : ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            
            if statusCode == 200{
                let status = response!.value(forKey: "status")as? Bool
                let arrResult = response!.value(forKey: "data")as? NSArray
                
                if error == nil
                {
                    self.arrAddressList.removeAll()
                    for obj in arrResult! {
                        let dicData = AddressListModelData(fromDictionary: (obj as? NSDictionary)!)
                        self.arrAddressList.append(dicData)
                    }
                    
                    if self.arrAddressList.count < 0 || self.arrAddressList.count == 0{
                        self.consHeight.constant = 90
                        self.pageControlAddress.isHidden = true
                    }else{
                        self.consHeight.constant = 190
                        self.pageControlAddress.isHidden = false
                    }
                    
                    self.collViewAddress.reloadData()
                }
                else
                {
                    self.arrAddressList.removeAll()
                    self.collViewAddress.reloadData()
                }
            }
            else
            {
                self.arrAddressList.removeAll()
                self.collViewAddress.reloadData()
            }
        }
    }
    
    func callingGetPetListAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let dicData = appDelegate.dicLoginUserDetails
        
        let ID = dicData.id
        
        let id = ID ?? ""
        let form_data = "?user_id="
        let URL = "\(GET_PET)\(form_data)\(id)"
        let param = ["" : ""]
        
        print(param)
        print(URL)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        
                        self.arrPetData.removeAll()
                        for obj in arrResult {
                            let dicData = Result(fromDictionary: (obj as? NSDictionary)!)
                            self.arrPetData.append(dicData)
                        }
                        
                        self.collView.reloadData()
                    }
                }
                else
                {
                    self.arrPetData.removeAll()
                    self.collViewAddress.reloadData()
                }
                GIFHUD.shared.dismiss()
            }
            else
            {
                GIFHUD.shared.dismiss()
                self.arrPetData.removeAll()
                self.collViewAddress.reloadData()
            }
        }
    }
    
    func callingOrderListAPI() {
        
        
        let dicData = appDelegate.dicLoginUserDetails
        
        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                            
                            if self.arrOrderList.count > 0{
                                let arrCount = self.arrOrderList.count
                                self.lblCartCount.text = "\(arrCount)"
                            }
                        }
                        if arrResult.count == 0
                        {
                            self.lblCartCount.text = "0"
                        }
                        
                    }else{
                    }
                }
                
            }
            else
            {
            }
        }
    }
    
    // MARK: - Camera & Photo Picker
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.mediaTypes = [kUTTypeImage as String]
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            self.view.makeToast("You don't have camera")
        }
    }
    
    func openGallary()
    {
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func choosePicture() {
        let alert  = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .overCurrentContext
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        let popoverController = alert.popoverPresentationController
        
        popoverController?.permittedArrowDirections = .up
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String], completion: @escaping () -> ()) {
        
        
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "profile", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
            
            let responseDict = ((data.value as AnyObject) as? NSDictionary)
            
            self.view.makeToast(responseDict?.value(forKey: "data") as? String)
            
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                print("upload success result: \(resut)")
                
            case .failure(let err):
                GIFHUD.shared.dismiss()
                print("upload erreot: \(err)")
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
            
            let dicData = appDelegate.dicLoginUserDetails
            
            let user_id = dicData.id
            
            let param = ["id": user_id ?? ""]
            
            //                uploadPhoto(BASE_URL + UPLOAD_IMAGE, image: image, params: param, header: ["" :""]) {
            //                                }
            
            self.myImageUploadRequest(user_id: user_id!)
            
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
            
            let dicData = appDelegate.dicLoginUserDetails
            
            let user_id = dicData.id
            
            let param = ["id": user_id ?? ""]
            self.myImageUploadRequest(user_id: user_id!)
            
            //                uploadPhoto(BASE_URL + UPLOAD_IMAGE, image: image, params: param,  header: ["" :""]) {
            //
            //                }
            
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func myImageUploadRequest(user_id: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let myUrl = NSURL(string: BASE_URL + UPLOAD_IMAGE);
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let param = ["id": user_id]
        
        let boundary = generateBoundaryString()
        request.timeoutInterval = 15
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = self.selectedImage.jpegData(compressionQuality: 0.6)
        if imageData == nil  {
            return
        }
        
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "profile", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            APIClient.sharedInstance.hideIndicator()
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            print("response = \(response!)")
            
            //print reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("response data = \(responseString!)")
            
            DispatchQueue.main.async {
                do{
                    let dict = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    print(dict)
                    
                    let strImage = dict.value(forKey: "field") as! String
                    
                    let registerData = appDelegate.dicLoginUserDetails
                    registerData.profile = strImage
                    
                    appDelegate.dicLoginUserDetails = registerData
                    
                    appDelegate.saveCurrentUserData(dic: appDelegate.dicLoginUserDetails)
                    
                    
                }catch{}
            }
            
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        let filename = "file.jpg"
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        
        
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}
extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
