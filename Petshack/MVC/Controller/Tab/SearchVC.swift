//
//  SearchVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class SearchVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //MARK: - Collection view flowOut
    
    let sectionInsets = UIEdgeInsets(top: 0,
                                     left: 0,
                                     bottom: 0,
                                     right: 0)
    let itemsPerRow: CGFloat = 2
    
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 200)
        
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        // edit properties here
        
        return _flowLayout
    }
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collView: UICollectionView!
    
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()
    
    //MARK: - Variables
    
    var searchActive : Bool = false
    
    var data = ["San Francisco","New York","San Jose","Chicago","Los Angeles","Austin","Seattle"]
    var filtered:[String] = []
    var arrSearchData : [Product] = [Product]()
    
    var arrWishlist : [GetWishListModelResult] = [GetWishListModelResult]()
    

    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shadowView()
                
        collView.delegate = self
        collView.dataSource = self
        self.collView.collectionViewLayout = flowLayout
        
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.callingOrderListAPI()
    }
    
    //MARK: - User Function
    
    func shadowView(){
        callingWishListAPI()
        
        lblCartCount.layer.cornerRadius = lblCartCount.frame.height/2
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height/2
        
        txtSearch.setLeftPaddingPoints(17)
        viewShadow.layer.shadowOffset = CGSize(width: 2, height: 1)
        viewShadow.layer.cornerRadius = 25
        viewShadow.layer.shadowRadius = 10
        viewShadow.layer.shadowColor = UIColor.init(red: 08.0/255.0, green: 16.0/255.0, blue: 24.0/255.0, alpha: 1).cgColor
        viewShadow.layer.shadowOpacity = 0.2
        
    }
    
    //MARK: - text-field searching
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSearch.resignFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        callingSearchDataAPI()
    }
    
    
    //MARK: - collection view delegate and datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSearchData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "SearchCell", for: indexPath)as! SearchCell
        
        cell.viewBackground.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        cell.viewBackground.layer.shadowRadius = 0
        cell.viewBackground.layer.shadowColor = UIColor.init(red: 08.0/255.0, green: 16.0/255.0, blue: 24.0/255.0, alpha: 1).cgColor
        cell.viewBackground.layer.shadowOpacity = 0.1
        
        let arrSearch = arrSearchData[indexPath.row]
        cell.lblName.text = arrSearch.productName
        
        let placeHolderImage = UIImage(named: "logo")
        let images = arrSearch.img ?? ""
        if let img = images as? String{
            let image = "\(image_path_product)\(img)"
            
            let url = URL(string: image)
            cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            cell.imgProduct.image = UIImage(named: "logo")
        }
        
        cell.lblTypes.text = "Biskut under"
       
        let price = arrSearch.price ?? ""
        let discount_price = arrSearch.discountPrice ?? ""
        let strPrice = Float(price)
        let discount_amount1 = Float(discount_price)
        
        let subtraction = "\(strPrice! - discount_amount1!)"
        let sub_value = Float(subtraction)
        cell.lblPrice.text = "₹ \(subtraction)"
        cell.lblOldPrice.attributedText = "₹ \(price)".strikeThrough()
        
        cell.btnClicked.tag = indexPath.row
        cell.btnClicked.addTarget(self, action: #selector(clickedWish(sender:)), for: .touchUpInside)

        
        for objectData in arrWishlist
        {
            if objectData.productId == arrSearch.id
            {
                cell.btnWish.setImage(UIImage(named: "ic_fill_heart"), for: .normal)
            }
            else
            {
                cell.btnWish.setImage(UIImage(named: "heart1"), for: .normal)
            }
        }
        
        
        return cell
    }
        
    @objc func clickedWish(sender: UIButton)
    {
        let arrSearch = arrSearchData[sender.tag]
     
        let p_id = arrSearch.id ?? ""
        let productName = arrSearch.productName ?? ""
        
        
        callingAddWishAPI(productId: p_id, productName: productName)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "ProductDetailVC")as! ProductDetailVC
        let arrDeatil = self.arrSearchData[indexPath.row]
        vc.dicResult = arrDeatil
        vc.isFromSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedMenu(_ sender: Any) {
        
        self.sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    
    @IBAction func clickedLike(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "FavoriteVC")as! FavoriteVC
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    @IBAction func clickedNotify(_ sender: Any) {
        
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
        
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "MyCartVC")as! MyCartVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    //MARK: - api calling
    func callingSearchDataAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let text = txtSearch.text!
        let form_data = "?search="
        let URL = "\(SEARCH_PRODUCT)\(form_data)\(text)"
        
        let param = ["" : ""]
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderNewGet(URL, parameters: param) { (response, error, statusCode) in
            
            if error == nil
            {
                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                
                if statusCode == 200{
                                        
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "product")as! NSArray
                    
                    if status == true{
                        self.arrSearchData.removeAll()
                        for obj in arrResult {
                            let dicData = Product(fromDictionary: (obj as? NSDictionary)!)
                            self.arrSearchData.append(dicData)
                        }
                        self.collView.reloadData()
                        
                    }
                    else
                    {
                        self.arrSearchData.removeAll()
                        self.collView.reloadData()
                    }
                    
                }
                else
                {
                    
                    self.arrSearchData.removeAll()
                    self.collView.reloadData()
                }
                GIFHUD.shared.dismiss()
            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
    
    
    func callingOrderListAPI() {
        
      
        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                            
                           if self.arrOrderList.count > 0{
                                let arrCount = self.arrOrderList.count
                                self.lblCartCount.text = "\(arrCount)"
                            }
                        }
                        
                        if arrResult.count == 0
                        {
                            self.lblCartCount.text = "0"
                        }
                        
                    }else{
                    }
                }

            }
            else
            {
            }
        }
    }
    
    func callingAddWishAPI(productId: String,productName: String ) {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let dicData = appDelegate.dicLoginUserDetails

        let user_id = dicData.id
        
        let param = ["user_id" : user_id ?? "","product_id" : productId,"name": productName ?? ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(WISH_ADD, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Int
                let responseMsg = response?.value(forKey: "message") as? String

                if success == 1
                {
                    
                    self.callingWishListAPI()
                }
                else
                {
                    self.view.makeToast(responseMsg)
                }
                
                GIFHUD.shared.dismiss()

            }
            else
            {
                GIFHUD.shared.dismiss()
                let responseMsg = response?.value(forKey: "message") as? String

                self.view.makeToast(responseMsg)
            }
        }
    }
    
    func callingWishListAPI() {

        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)

        let dicData = appDelegate.dicLoginUserDetails

        let user_id = dicData.id

        let id = user_id ?? ""
        let param = ["user_id" : user_id ?? ""]
        print(param)

        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(WISH_LIST, parameters: param) { (response, error, statusCode) in

            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")

            if error == nil
            {
                if statusCode == 200{

                    let status = response!.value(forKey: "status")as? Bool
                    let arrResult = response!.value(forKey: "result") as? NSArray

                    if status == true{
                        
                        for obj in arrResult! {
                            let dicData = GetWishListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrWishlist.append(dicData)
                        }
                        
                        self.callingSearchDataAPI()
                    }
                    GIFHUD.shared.dismiss()
                    self.collView.reloadData()
                }
                else
                {
                    GIFHUD.shared.dismiss()
                }
            }
        }

    }
    
}
