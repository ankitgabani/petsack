//
//  SentOtpVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 14/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD
import GoogleSignIn
import FirebaseAuth
import AuthenticationServices
import CryptoKit
import FBSDKLoginKit
import FBSDKCoreKit

class SentOtpVC: UIViewController, UITextFieldDelegate,GIDSignInDelegate, ASAuthorizationControllerPresentationContextProviding,ASAuthorizationControllerDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var btnCreateProfile: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    
    var selectedEmail = ""
    var selectedName = ""
    var selectedPhone = ""
    
    var isShowBackButton = false
    
    var isFromSide = false
    
    var isLoginType = ""
    fileprivate var currentNonce: String?
    
    //MARK: - variables
    
    var otp = Int()
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtMobileNumber.delegate = self
        txtMobileNumber.setLeftPaddingPoints(10)
        txtMobileNumber.becomeFirstResponder()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isShowBackButton == true
        {
            btnBack.isHidden = false
        }
        else
        {
            btnBack.isHidden = true
        }
     
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: - Text-Field Validation
    func isValidatedLogin() -> Bool {
        
        if txtMobileNumber.text == "" {
            self.view.makeToast("mobile number not valid")
            return false
        }
        return true
    }
    
    @IBAction func clickedApple(_ sender: Any) {
        
        self.startSignInWithAppleFlow()
    }
    
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
        Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            let fullName = appleIDCredential.fullName
            
            var firstName = fullName?.givenName ?? fullName?.familyName
            var LastName  = fullName?.familyName ?? fullName?.givenName
            
            
            // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            // Sign in with Firebase.
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if (error != nil) {
                    print(error!.localizedDescription)
                    return
                }
                else{
                    let additionalInfo = authResult?.additionalUserInfo?.profile
                    
                    let email = additionalInfo?["email"] as? String
                    
                    self.selectedEmail = email ?? ""
                    self.selectedName = fullName.debugDescription
                    
                    self.callingUserDataAPI()
                }
                
            }
        }
        
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            // Handle error.
            print("Sign in with Apple errored: \(error)")
        }
        
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedBack(_ sender: Any) {
        
        if isFromSide == true
        {
            appDelegate.setUpSideMenu()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    @IBAction func clickedCreateProfile(_ sender: Any) {
        
        if isValidatedLogin(){
            callingSentOtpAPI()
        }
        
    }
    
    @IBAction func clickedSkipLogin(_ sender: Any) {
        appDelegate.setUpSideMenu()
    }
    
    @IBAction func clickedGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        // let userId = user.userID                  // For client-side use only!
        // let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        selectedEmail = email ?? ""
        selectedName = fullName ?? ""
        
        callingUserDataAPI()
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    @IBAction func clickedFacebook(_ sender: Any) {
        loginWithFacebook()
    }
    
    //MARK:- Facebook Login
    func loginWithFacebook() {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { result, error in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
        
    }
    
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result!)
                    var strEmail = String()
                    if let email = (result as! NSDictionary).value(forKey: "email") as? String {
                        self.selectedEmail = email ?? ""
                    }
                    let strid = (result as! NSDictionary).value(forKey: "id") as! String
                    let name = (result as! NSDictionary).value(forKey: "name") as! String
                    let pictureDic = (result as! NSDictionary).value(forKey: "picture") as! NSDictionary
                    let dataDic = pictureDic.value(forKey: "data") as! NSDictionary
                    
                    let picture = dataDic.value(forKey: "url") as! String
                    
                    self.selectedName = name
                    
                    print(strEmail)
                    print(name)
                    
                    self.callingUserDataAPI()
                    
                } else {
                    print(error?.localizedDescription)
                }
            })
        }
    }
    
    //MARK: - API Calling
    
    func callingSentOtpAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let phone_no = txtMobileNumber.text!
        let form_data = "?phone="
        let URL = "\(SEND_OTP)\(form_data)\(phone_no)"
        
        let param = ["" : ""]
        //print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Int
                let responseMsg = response?.value(forKey: "message") as? String
                self.otp = Int(truncating: (response?.value(forKey: "otp") as? NSNumber)!)
                
                if success == 1{
                    let vc = self.storyboard?.instantiateViewController(identifier: "OTPVerificationVC")as! OTPVerificationVC
                    vc.otp = self.otp
                    vc.mobile_number = phone_no
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.view.makeToast(responseMsg)
                }
                
                GIFHUD.shared.dismiss()
                
            }
            else
            {
                let responseMsg = response?.value(forKey: "message") as? String
                
                self.view.makeToast(responseMsg)
                GIFHUD.shared.dismiss()
                
            }
        }
    }
    
    
    func callingUserDataAPI() {

        APIClient.sharedInstance.showIndicator()
        
        let device_id = UIDevice.current.identifierForVendor?.uuidString
        
        let firebaseToken = UserDefaults.standard.value(forKey: "FirebaseToken") as? String
        
        let param = ["device_id" : device_id ?? "","device_type" : "iOS","device_token": firebaseToken ?? "123","email_phone": "\(selectedEmail)"]
        //print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_DATA, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                
                let success = response?.value(forKey: "status") as? Bool
                let responseMsg = response?.value(forKey: "message") as? String
                let arrResult = response?.value(forKey: "result") as?  NSDictionary
                
                if success!{
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    appDelegate.saveCurrentUserData(dic: RegisterDataResult(fromDictionary: arrResult!))
                    appDelegate.dicLoginUserDetails = RegisterDataResult(fromDictionary: arrResult!)
                    UserDefaults.standard.setValue(true, forKey: "UserLogin")
                    UserDefaults.standard.synchronize()
                    appDelegate.setUpSideMenu()
                }
                else
                {
                    self.CreateUserAPI()
                }
            }
            else
            {
                
                self.CreateUserAPI()
             }
        }
    }
    
    func CreateUserAPI() {
        
        var param = ["":""]
        
        param = ["phone" : self.selectedEmail,"name" : self.selectedName,"email" : self.selectedEmail,"password" : self.selectedEmail,"id":""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_CREATE, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
           
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Int
                let responseMsg = response?.value(forKey: "msg") as? String
                
                
                self.callingLoginAPI()
                self.view.makeToast(responseMsg)

            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
                let responseMsg = response?.value(forKey: "msg") as? String

                self.view.makeToast(responseMsg)
            }
        }
    }
    
    func callingLoginAPI() {
        
        let device_id = UIDevice.current.identifierForVendor?.uuidString
        
        let firebaseToken = UserDefaults.standard.value(forKey: "FirebaseToken") as? String
        
        let param = ["device_id" : device_id ?? "","device_type" : "iOS","device_token": firebaseToken ?? "123","email_phone": self.selectedEmail]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_DATA, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
          
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Bool
                let responseMsg = response?.value(forKey: "message") as? String
                let arrResult = response?.value(forKey: "result") as?  NSDictionary
                
                if success!{
                    APIClient.sharedInstance.hideIndicator()

                    appDelegate.saveCurrentUserData(dic: RegisterDataResult(fromDictionary: arrResult!))
                    appDelegate.dicLoginUserDetails = RegisterDataResult(fromDictionary: arrResult!)
                    UserDefaults.standard.setValue(true, forKey: "UserLogin")
                    UserDefaults.standard.synchronize()
                    appDelegate.setUpSideMenu()
                    
                }else{
                    APIClient.sharedInstance.hideIndicator()

                    self.view.makeToast(responseMsg)
                }
                GIFHUD.shared.dismiss()

            }
            else
            {
                APIClient.sharedInstance.hideIndicator()

                GIFHUD.shared.dismiss()
                let responseMsg = response?.value(forKey: "message") as? String

                self.view.makeToast(responseMsg)
            }
        }
    }
    
}


//MARK: - TextView Extention

extension UITextView {
    
    private class PlaceholderLabel: UILabel { }
    
    private var placeholderLabel: PlaceholderLabel {
        if let label = subviews.compactMap( { $0 as? PlaceholderLabel }).first {
            return label
        } else {
            let label = PlaceholderLabel(frame: .zero)
            label.font = font
            addSubview(label)
            return label
        }
    }
    
    @IBInspectable
    var placeholder: String {
        get {
            return subviews.compactMap( { $0 as? PlaceholderLabel }).first?.text ?? ""
        }
        set {
            let placeholderLabel = self.placeholderLabel
            placeholderLabel.text = newValue
            placeholderLabel.textColor = UIColor.gray
            placeholderLabel.numberOfLines = 0
            let width = frame.width - textContainer.lineFragmentPadding * 2
            let size = placeholderLabel.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))
            placeholderLabel.frame.size.height = size.height
            placeholderLabel.frame.size.width = width
            placeholderLabel.frame.origin = CGPoint(x: textContainer.lineFragmentPadding, y: textContainerInset.top)
            
            textStorage.delegate = self
        }
    }
    
}

extension UITextView: NSTextStorageDelegate {
    
    public func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
        if editedMask.contains(.editedCharacters) {
            placeholderLabel.isHidden = !text.isEmpty
        }
    }
    
}
