//
//  UserDetailVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 14/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class UserDetailVC: UIViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlet
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    
    var mobile_number = String()
    var isFromSocialLogin = false
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFullName.delegate = self
        txtMobile.delegate = self
        txtEmail.delegate = self
        
        txtFullName.setLeftPaddingPoints(10)
        txtMobile.setLeftPaddingPoints(10)
        txtEmail.setLeftPaddingPoints(10)
        
        print(self.mobile_number)
        
        if isFromSocialLogin == true
        {
            txtEmail.text = self.mobile_number
        }
        else
        {
            txtMobile.text = self.mobile_number
        }
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    
    //MARK: - Text-Field Validation
    func isValidatedLogin() -> Bool {
        if txtFullName.text == "" {
            self.view.makeToast("Full Name not valid")
            return false
        } else if txtMobile.text == "" {
            self.view.makeToast("Mobile Number not valid")
            return false
        } else if txtEmail.text == "" {
            self.view.makeToast("Email not valid")
            return false
        }
        return true
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        switch textField {
        case txtEmail:
            txtFullName.becomeFirstResponder()
        case txtFullName:
            txtMobile.becomeFirstResponder()
        default:
            txtMobile.resignFirstResponder()
        }
        return true
    }

    
    
    //MARK: - IBAction
    
    @IBAction func clickedRegister(_ sender: Any) {
        
        if isValidatedLogin(){
            CreateUserAPI()
        }
        
    }
    
    //MARK: - API Calling:-
    
    func CreateUserAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        var param = ["":""]
        
        if isFromSocialLogin == true
        {
            param = ["phone" : "\(txtMobile.text ?? "")","name" : txtFullName.text!,"email" : "\(mobile_number)","password" : "\(mobile_number)","id":""]
        }
        else
        {
            param = ["phone" : "\(mobile_number)","name" : txtFullName.text!,"email" : txtEmail.text!,"password" : "\(mobile_number)","id":""]
        }
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_CREATE, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
           
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Int
                let responseMsg = response?.value(forKey: "msg") as? String
                
                
                self.callingLoginAPI()
                self.view.makeToast(responseMsg)
                GIFHUD.shared.dismiss()

            }
            else
            {
                let responseMsg = response?.value(forKey: "msg") as? String

                GIFHUD.shared.dismiss()
                self.view.makeToast(responseMsg)
            }

        }
    }
    
    //MARK: - API Calling
    
    func callingLoginAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let device_id = UIDevice.current.identifierForVendor?.uuidString
        
        let firebaseToken = UserDefaults.standard.value(forKey: "FirebaseToken") as? String
        
        let param = ["device_id" : device_id ?? "","device_type" : "iOS","device_token": firebaseToken ?? "123","email_phone": "\(mobile_number)"]
        //print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_DATA, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
          
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Bool
                let responseMsg = response?.value(forKey: "message") as? String
                let arrResult = response?.value(forKey: "result") as?  NSDictionary
                
                if success!{
                    
                    appDelegate.saveCurrentUserData(dic: RegisterDataResult(fromDictionary: arrResult!))
                    appDelegate.dicLoginUserDetails = RegisterDataResult(fromDictionary: arrResult!)
                    UserDefaults.standard.setValue(true, forKey: "UserLogin")
                    UserDefaults.standard.synchronize()
                    appDelegate.setUpSideMenu()
                    
                }else{
                    self.view.makeToast(responseMsg)
                }
                GIFHUD.shared.dismiss()

            }
            else
            {
                GIFHUD.shared.dismiss()
                let responseMsg = response?.value(forKey: "message") as? String

                self.view.makeToast(responseMsg)
            }
        }
    }
    
    
}
