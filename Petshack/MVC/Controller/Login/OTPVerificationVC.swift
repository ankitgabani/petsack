//
//  OTPVerificationVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 14/09/21.
//

import UIKit
import SVProgressHUD
import GiFHUD_Swift

class OTPVerificationVC: UIViewController, OTPFieldViewDelegate {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var otpTextFieldView: OTPFieldView!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    
    //MARK: - variables
    var count = 60
    var timer : Timer?
    var otp = Int()
    var mobile_number = String()
    
    var strCoustmOTP = ""
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        setupOtpView()
        GIFHUD.shared.dismiss()
        self.navigationController?.navigationBar.isHidden = true
        
        self.timer?.invalidate()
        self.count = 60
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(OTPVerificationVC.update), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - SET-UP OTPVIEW
    
    func setupOtpView(){
        
        self.otpTextFieldView.fieldsCount = 6
        self.otpTextFieldView.cursorColor = UIColor.white
        self.otpTextFieldView.tintColor = UIColor.black
        self.otpTextFieldView.displayType = .underlinedBottom
        self.otpTextFieldView.fieldSize = 42
        self.otpTextFieldView.separatorSpace = 10
        self.otpTextFieldView.shouldAllowIntermediateEditing = false
        self.otpTextFieldView.delegate = self
        self.otpTextFieldView.initializeUI()
    }
    
    //MARK: - textfield delegate
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        self.strCoustmOTP = otpString
        // 87  authWithVerificationCode(otpString: otpString)
    }
    
    @objc func update() {
        if(count > 0) {
            count-=1
            btnResend.isUserInteractionEnabled = false
            btnResend.setTitle("00:\(count)", for: .normal)
        }else{
            timer?.invalidate()
            btnResend.isUserInteractionEnabled = true
            self.callingSentOtpAPI()
        }
    }
    
    
    //MARK: - IBAction
    
    @IBAction func clickedResend(_ sender: Any) {
    }
    
    @IBAction func clickedVerify(_ sender: Any) {
        callingSentOtpAPI()
    }
    
    //MARK: - API Calling
    
    func callingSentOtpAPI() {
        
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let param = ["otp" : self.strCoustmOTP,"phone" : "\(self.mobile_number)"]
        //print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(VERIFY_OTP, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
          
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Int
                let responseMsg = response?.value(forKey: "message") as? String
                let arrResult = response?.value(forKey: "result") as? NSArray
                
                if success == 1{
                    
                    if arrResult != nil{
                        self.view.makeToast(responseMsg)
                        
                        let dicData = arrResult?[0] as? NSDictionary

                        appDelegate.saveCurrentUserData(dic: RegisterDataResult(fromDictionary: dicData!))
                        appDelegate.dicLoginUserDetails = RegisterDataResult(fromDictionary: dicData!)
                        
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                        appDelegate.setUpSideMenu()
                        
                    }else{
                        
                        let vc = self.storyboard?.instantiateViewController(identifier: "UserDetailVC")as! UserDetailVC
                        vc.mobile_number = self.mobile_number
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }else{
                    self.view.makeToast(responseMsg)
                }
                
                GIFHUD.shared.dismiss()

            }
            else
            {
                GIFHUD.shared.dismiss()
                let responseMsg = response?.value(forKey: "message") as? String

                self.view.makeToast(responseMsg)
            }
        }
    }
    
    
}


