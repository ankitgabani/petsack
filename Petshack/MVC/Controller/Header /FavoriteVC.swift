//
//  FavoriteVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 29/09/21.
//

import UIKit
import GiFHUD_Swift

class FavoriteVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //MARK: - Collection view flowOut
    @IBOutlet weak var imgNoPro: UIImageView!
    
    let sectionInsets = UIEdgeInsets(top: 0,
                                     left: 0,
                                     bottom: 0,
                                     right: 0)
    let itemsPerRow: CGFloat = 2
    
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 200)
        
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        // edit properties here
        
        return _flowLayout
    }
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var collView: UICollectionView!
    
    var arrWishlist : [GetHomeDataFeaturedProduct] = [GetHomeDataFeaturedProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgNoPro.isHidden = true
        collView.delegate = self
        collView.dataSource = self
        self.collView.collectionViewLayout = flowLayout
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callingWishListAPI()
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    //MARK: - collection view delegate and datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWishlist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "FavoriteCell", for: indexPath)as! FavoriteCell
        
        let dicData = arrWishlist[indexPath.row]
        
        let name = dicData.productName ?? ""
        cell.lblName.text = name
        
        let images = dicData.img ?? ""
        let placeHolderImage = UIImage(named: "logo")
        if let img = images as? String{
            let image = "\(image_path_product)\(img)"

            let url = URL(string: image)
            cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            cell.imgProduct.image = UIImage(named: "logo")
        }

        let price = dicData.price ?? ""
        let discount_price = dicData.discountPrice ?? ""
        let strPrice = Float(price)
        let discount_amount1 = Float(discount_price)
        
        let subtraction = "\(strPrice! - discount_amount1!)"
        let sub_value = Float(subtraction)
        cell.lblPrice.text = "₹ \(subtraction)"
        cell.lblOldPrice.attributedText = "₹ \(price)".strikeThrough()

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    //MARK: - IBAction
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: - API Calling
    
    func callingWishListAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let dicData = appDelegate.dicLoginUserDetails

        let user_id = dicData.id
        
        let id = user_id ?? ""
        let param = ["user_id" : user_id ?? ""]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(WISH_LIST, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as? Bool
                    let arrObj = response!.value(forKey: "result") as? NSArray
                    let msg = response!.value(forKey: "msg")as? String
                    
                    if status == true{
                        for obj in arrObj! {
                            let dicData = GetHomeDataFeaturedProduct(fromDictionary: (obj as? NSDictionary)!)
                            self.arrWishlist.append(dicData)
                        }
                    }
                    GIFHUD.shared.dismiss()
                    self.collView.reloadData()
                }
                else
                {
                    GIFHUD.shared.dismiss()
                }
            }
        }
        
    }
}
