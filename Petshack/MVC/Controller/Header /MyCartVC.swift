//
//  MyCartVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 16/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class MyCartVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate  {
    
    //MARK: - IBAction
    
    @IBOutlet weak var btnCheckout: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib(nibName: "MyCartCell", bundle: nil), forCellReuseIdentifier: "MyCartCell")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callingOrderListAPI(isShow: true)
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: - tableview delegate-datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MyCartCell", for: indexPath) as! MyCartCell
        
        cell.selectionStyle = .none
        
        let arrOrder = arrOrderList[indexPath.row]
        
        let name = arrOrder.name ?? ""
        cell.lblName.text = name
        
        let placeHolderImage = UIImage(named: "logo")
        let images = arrOrder.img ?? ""
        if let img = images as? String{
            let image = "\(image_path_product)\(img)"
            
            let url = URL(string: image)
            cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            cell.imgProduct.image = UIImage(named: "logo")
        }
        
        let price = arrOrder.subtotal ?? 0
        cell.lblPrice.text = "₹ \(price)"
        
        let count = arrOrder.qty ?? 0
        if count == 0
        {
            cell.lblCount.text = "1"
        }
        else
        {
            cell.lblCount.text = "\(count)"
        }
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(clickedplus(sender:)), for: .touchUpInside)
        
        cell.btnSubtract.tag = indexPath.row
        cell.btnSubtract.addTarget(self, action: #selector(clickedMinus(sender:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(clickedDelete(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    
    //MARK: - button Action
    @objc func clickedplus(sender: UIButton) {
        
        let objslider = arrOrderList[sender.tag]
        
        let cartID = objslider.id ?? ""
        
        guard let cell = sender.superview?.superview?.superview as? MyCartCell else {
            return // or fatalError() or whatever
        }
        
        let totalquantity = Int(cell.lblCount.text!)
        
        if totalquantity! > 0 {
            let newValue = totalquantity! + 1
            
            cell.lblCount.text = "\(newValue)"
            
            callingUpdateCartAPI(id : cartID,qty: cell.lblCount.text!)
        }
        
    }
    
    @objc func clickedMinus(sender: UIButton){
        
        
        let objslider = arrOrderList[sender.tag]
        
        let cartID = objslider.id ?? ""
        
        guard let cell = sender.superview?.superview?.superview as? MyCartCell else {
            return // or fatalError() or whatever
        }
        
        let totalquantity = Int(cell.lblCount.text!)
        
        if totalquantity! > 0 {
            let newValue = totalquantity! - 1
            
            cell.lblCount.text = "\(newValue)"
            
            if newValue > 0{
                callingUpdateCartAPI(id : cartID,qty: cell.lblCount.text!)
            }else{
                
                callingUpdateCartAPI(id : cartID,qty: cell.lblCount.text!)
            }
            
        }
    }
    
    @objc func clickedDelete(sender: UIButton){
        
        let objslider = arrOrderList[sender.tag]
        
        let cartID = objslider.id ?? ""
        callingRemoveCartAPI(id: cartID)
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 1 {
            
            alertView.dismiss(withClickedButtonIndex: 1, animated: true)
            
            let objSlider = alertView.accessibilityElements?.first as! OrderListModelResult
            let cell = alertView.accessibilityElements![1] as! MyCartCell
            
            callingUpdateCartAPI(id : objSlider.id,qty: cell.lblCount.text!)
            
        }
    }
    
    
    //MARK: - IBAction
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedCheckout(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutOrderVC")as! CheckoutOrderVC
        vc.arrOrderList = self.arrOrderList
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: - API calling
    
    func callingOrderListAPI(isShow: Bool) {
        
        if isShow == true
        {
            GIFHUD.shared.setGif(named: "splash.gif")
            GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
            GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        }
        
        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                GIFHUD.shared.dismiss()
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            self.btnCheckout.isHidden = false
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                        }
                        
                        if arrResult.count == 0
                        {
                            self.btnCheckout.isHidden = true
                        }
                        
                    }else{
                        self.btnCheckout.isHidden = true
                    }
                    self.tblView.reloadData()
                }
                GIFHUD.shared.dismiss()
                
            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
    
    func callingUpdateCartAPI(id : String,qty: String) {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let dicData = appDelegate.dicLoginUserDetails

        let email = dicData.email ?? ""
        let uid = dicData.id ?? ""
        
        let param = ["user_id" : uid,"id" : id,"qty" : qty]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(UPDATE_CART, parameters: param) { (response, error, statusCode) in
            
            
            
            if error == nil
            {
                let status = response!.value(forKey: "status")as! Bool
                let msg = response!.value(forKey: "result")as! String
                if statusCode == 200{
                    
                    
                    if status == true {
                        DispatchQueue.main.async {
                            self.view.makeToast(msg)
                            
                            self.callingOrderListAPI(isShow: false)
                        }
                    }else{
                        self.view.makeToast(msg)
                        self.callingOrderListAPI(isShow: false)
                    }
                }
                else
                {
                    self.view.makeToast(msg)
                }
                GIFHUD.shared.dismiss()
            }else{
                let msg = response!.value(forKey: "result")as! String

                self.view.makeToast(msg)
                GIFHUD.shared.dismiss()
            }
        }
    }
    
    func callingRemoveCartAPI(id : String) {
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let dicData = appDelegate.dicLoginUserDetails

        let email = dicData.email ?? ""
        let uid = dicData.id ?? ""
        
        let param = ["user_id" : uid,"id" : id]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(REMOVE_CART, parameters: param) { (response, error, statusCode) in
            
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                let status = response!.value(forKey: "status")as! Bool
                let msg = response!.value(forKey: "result")as! String

                if statusCode == 200{
                    
                    if status == true {
                        DispatchQueue.main.async {
                            self.view.makeToast(msg)
                            self.callingOrderListAPI(isShow: false)
                        }
                    }else{
                        self.view.makeToast(msg)
                        self.callingOrderListAPI(isShow: false)
                    }
                    self.tblView.reloadData()
                }
                else
                {
                    self.view.makeToast(msg)
                }
                GIFHUD.shared.dismiss()
                
            }else{
                let msg = response!.value(forKey: "result")as! String

                self.view.makeToast(msg)
                GIFHUD.shared.dismiss()
            }
        }
    }
}
