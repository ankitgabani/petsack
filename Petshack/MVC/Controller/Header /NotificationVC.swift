//
//  NotificationVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 14/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - IBOutlet
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: - variables
    var arrOrderList : [OrderListTabOrder] =  [OrderListTabOrder]()
    
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callingGetOrderListAPI()
        tblView.delegate = self
        tblView.dataSource = self
        tblView.register(UINib(nibName: "MyOrderCell", bundle: nil), forCellReuseIdentifier: "MyOrderCell")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MyOrderCell", for: indexPath) as! MyOrderCell
        
        cell.selectionStyle = .none
        
        let dicData = self.arrOrderList[indexPath.row]
        let id = dicData.orderId ?? ""
        cell.lblOrderNo.text = id
        
        let price = dicData.total ?? ""
        cell.lblPrice.text = "₹ \(price)"
        
        let mode = dicData.paymentMode ?? ""
        cell.lblMode.text = "Mode: \(mode)"
        
        let date = dicData.created ?? ""
        cell.lblDate.text = date
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "OrderDetailVC")as! OrderDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func callingGetOrderListAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        

        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let form_data = "?user_id="
        let URL = "\(ORDER_LIST_TAB)\(form_data)\(id)"
        let param = ["" : ""]
        
        print(param)
        print(URL)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let dicData = response!.value(forKey: "data")as! NSDictionary
                    let arrResult = dicData.value(forKey: "order")as! NSArray
                    
                    
                    if status == true{
                        
                        for obj in arrResult {
                            let dicData = OrderListTabOrder(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                        }
                        
                        self.tblView.reloadData()
                    }
                }
                else
                {
                    self.arrOrderList.removeAll()
                    self.tblView.reloadData()
                }
                GIFHUD.shared.dismiss()

            }
            else
            {
                self.arrOrderList.removeAll()
                self.tblView.reloadData()
                GIFHUD.shared.dismiss()
            }
        }
    }
    
}
