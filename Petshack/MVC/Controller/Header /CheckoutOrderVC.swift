//
//  CheckoutOrderVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 28/10/21.
//

import UIKit
import DLRadioButton

protocol PizzaDelegate
{
    func onPizzaReady(dicAddress: AddressListModelData)
}

class CheckoutOrderVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, PizzaDelegate{
  
   
    //MARK: - Collection view flowOut
    
    let sectionInsets = UIEdgeInsets(top: 0,
                                     left: 0,
                                     bottom: 0,
                                     right: 0)
    let itemsPerRow: CGFloat = 2
    
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        _flowLayout.itemSize = CGSize(width: 100, height: 135)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        // edit properties here
        
        return _flowLayout
    }
    
    @IBOutlet weak var btnCashDelivery: DLRadioButton!
    @IBOutlet weak var btnOnline: DLRadioButton!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblServiceCharge: UILabel!
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDeliveryCharge: UILabel!
    
    @IBOutlet weak var lblSelectAddress: UILabel!
    @IBOutlet weak var consAddress: NSLayoutConstraint!
    @IBOutlet weak var btnAddressType: UIButton!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var collView: UICollectionView!
    
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()
    
    var strAmount = 0
    var strDiscount = 0
    
    var arrAddress = AddressListModelData()
    
    var isPaymentCOD = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        for object in arrOrderList
        {
            let price = object.subtotal ?? 0
            strAmount = strAmount + price
            
            let discount = object.discount ?? ""
            let toInt = Int(discount) ?? 0
            strDiscount = strDiscount + toInt
        }
        self.lblAmount.text = "₹ \(strAmount)"
        self.lblDiscount.text = "₹ \(strDiscount)"
        
        let total = strAmount - strDiscount
        self.lblTotalAmount.text = "₹ \(total)"
        
        self.consAddress.constant = 50
        viewAddress.isHidden = true
        self.lblSelectAddress.isHidden = false
        
        collView.delegate = self
        collView.dataSource = self
        self.collView.collectionViewLayout = flowLayout
        
        btnOnline.isSelected = true
        btnOnline.setImage(UIImage(named: "ic_check"), for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func onPizzaReady(dicAddress: AddressListModelData) {
        
        
        self.consAddress.constant = 100
        self.lblSelectAddress.isHidden = true
        
        
        viewAddress.isHidden = false

        arrAddress = dicAddress
        
        let name = arrAddress.fname ?? ""
        lblName.text = name
        
        let street = arrAddress.street ?? ""
        let landmark = arrAddress.landmark ?? ""
        let city = arrAddress.city ?? ""
        let state = arrAddress.state ?? ""
        let country = arrAddress.country ?? ""
        let pincode = arrAddress.pincode ?? ""
      
        lblAddress.text = "\(street),Near \(landmark), \(city),\(state),\(country), \(pincode)"

        let phone = arrAddress.phone ?? ""
        lblMobile.text = phone
        
        let type = arrAddress.type ?? ""
        btnAddressType.setTitle(type, for: .normal)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrOrderList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "CheckoutOrderCell", for: indexPath)as! CheckoutOrderCell
        
        let arrOrder = self.arrOrderList[indexPath.row]
    
        cell.lblName.text = arrOrder.name ?? ""
        
        let placeHolderImage = UIImage(named: "logo")
        let images = arrOrder.img ?? ""
        if let img = images as? String{
            let image = "\(image_path_product)\(img)"
            
            let url = URL(string: image)
            cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            cell.imgProduct.image = UIImage(named: "logo")
        }
        
        
        return cell
    }
    
    @IBAction func clickedAddress(_ sender: Any) {
//
//        self.consAddress.constant = 100
//      //  self.viewAddress.isHidden = false
//        self.lblSelectAddress.isHidden = true
//
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
        controller.delegate = self
        //  controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
    //    controller.dicProduct = self.dicProduct
        self.present(controller, animated: true, completion: nil)
        
       
    }
    
    @IBAction func btnOnline(_ sender: Any) {
        
        isPaymentCOD = false
        
        btnOnline.isSelected = true
        btnOnline.setImage(UIImage(named: "ic_check"), for: .normal)
        
        btnCashDelivery.isSelected = false
        btnCashDelivery.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
    }
    
    @IBAction func btnCashOnDelivery(_ sender: Any) {
        
        isPaymentCOD = true
        
        btnCashDelivery.isSelected = true
        btnCashDelivery.setImage(UIImage(named: "ic_check"), for: .normal)
        
        btnOnline.isSelected = false
        btnOnline.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedPlaceOrder(_ sender: Any)
    {
        if viewAddress.isHidden == true
        {
            self.view.makeToast("Please select address")
        }
//        else if isPaymentCOD == false
//        {
//            self.view.makeToast("Please select Payment Method")
//        }
        else
        {
            callingOrderListAPI()
        }
    }
        
    func callingOrderListAPI() {
      

        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
                
        let id = ID ?? ""
        
        let fname = arrAddress.fname ?? ""
        let lname = arrAddress.lname ?? ""
        let email = arrAddress.email ?? ""
        let phone = arrAddress.phone ?? ""
        let country = arrAddress.country ?? ""
        let street = arrAddress.street ?? ""
        let landmark = arrAddress.landmark ?? ""
        let city = arrAddress.city ?? ""
        let state = arrAddress.state ?? ""
        let pincode = arrAddress.pincode ?? ""
        let address_type = arrAddress.type ?? ""

        
        var param = ["":""]
        
        if self.isPaymentCOD == true
        {
            param = ["fname" : fname,"lname": lname,"email": email,"phone": phone,"country": country,"street": street,"landmark": landmark,"city": city,"state": state,"pincode": pincode,"address_type": address_type,"reward": "0","payment_mode": "COD","transaction_id": "","payment_status": "0","user_id": id]
        }
        else
        {
            param = ["fname" : fname,"lname": lname,"email": email,"phone": phone,"country": country,"street": street,"landmark": landmark,"city": city,"state": state,"pincode": pincode,"address_type": address_type,"reward": "0","payment_mode": "Online","transaction_id": "KJDASG87555","payment_status": "1","user_id": id]
        }
        
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(PLACE_ORDER, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                  
                    appDelegate.setUpSideMenu()
                }

            }
            else
            {
            }
        }
    }

}
