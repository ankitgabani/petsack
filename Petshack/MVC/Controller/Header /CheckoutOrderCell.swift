//
//  CheckoutOrderCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 28/10/21.
//

import UIKit

class CheckoutOrderCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var imgProduct: UIImageView!
}
