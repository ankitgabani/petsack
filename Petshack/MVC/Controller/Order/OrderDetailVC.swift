//
//  OrderDetailVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 13/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class OrderDetailVC: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    //MARK: - IBOutlets

  
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblMode: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var lblAddName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblServiceCharge: UILabel!
    @IBOutlet weak var lblDeliveryCharge: UILabel!
    
    @IBOutlet weak var viewOrdered: UIView!
    @IBOutlet weak var viewAcceptCancelled: UIView!
    @IBOutlet weak var lblOrdered: UILabel!
    
    @IBOutlet weak var viewAccept: UIView!
    @IBOutlet weak var lblAccept: UILabel!
    @IBOutlet weak var viewOutDelivery: UIView!
    @IBOutlet weak var lblOutDelivery: UILabel!
    
    @IBOutlet weak var lblOrderAccept: UILabel!
    @IBOutlet weak var lblDescAccepted: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    
    @IBOutlet weak var viewDelivered: UIView!
    @IBOutlet weak var lblDescDelivery: UILabel!
    
    @IBOutlet weak var lblDelivered: UILabel!
    @IBOutlet weak var lblDescDelivered: UILabel!
    
    
    @IBOutlet weak var constOrderPlaced: NSLayoutConstraint!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    
    var isCancelled = false
    
    @IBOutlet weak var btnCancel: UIButton!
    var arrOrderDetail : OrderListTabOrder?
    
    var arrProduct : [OrderListTabProduct] = [OrderListTabProduct]()
    var dicProduct : OrderListTabProduct?
    
    var arrAddress  : [OrderListTabAddres] = [OrderListTabAddres]()
    var dicAddress : OrderListTabAddres?
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: - view cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    //MARK: - tableview delegate-datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "OrderDetailCell", for: indexPath) as! OrderDetailCell
        
        cell.selectionStyle = .none
        
        self.arrProduct = (arrOrderDetail?.product)!
        self.arrAddress = (arrOrderDetail?.address)!
        self.dicProduct = arrProduct[indexPath.row]
        self.dicAddress = arrAddress[0]
        
        let strImage = dicProduct!.img ?? ""
        let placeHolderImage = UIImage(named: "logo")
        if let img = strImage as? String{
            let image = "\(image_path_product)\(img)"

            let url = URL(string: image)
            cell.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            cell.imgProduct.image = UIImage(named: "logo")
        }
        
        let strName = dicProduct!.name ?? ""
        cell.lblName.text = strName
        
        let orderid  = arrOrderDetail?.orderId ?? ""
        self.lblOrderId.text = orderid
        
        let paymentMode  = arrOrderDetail?.paymentMode ?? ""
        self.lblMode.text = "Mode :\(paymentMode)"
        
        let date  = arrOrderDetail?.created ?? ""
        self.lblDate.text = date
        
        cell.btnReview.tag = indexPath.row
        cell.btnReview.addTarget(self, action: #selector(clickedReview(sender:)), for: .touchUpInside)
        
        let cancel_pro = arrOrderDetail?.cancelPro ?? ""
        if cancel_pro == "0"{
            self.btnStatus.setTitle("Placed", for: .normal)
            self.btnCancel.isHidden = false
            cell.btnReview.isHidden = false
            
            self.lblOrderAccept.text = "Order Accepted"
            self.lblDescAccepted.text = "Your Order has been Accepted"
            
            self.lblAccept.isHidden = false
            self.viewOutDelivery.isHidden = false
            self.lblDelivery.isHidden = false
            self.viewDelivered.isHidden = false
            
            self.lblOutDelivery.isHidden = false
            self.lblDescDelivery.isHidden = false
            self.lblDelivered.isHidden = false
            self.lblDescDelivered.isHidden = false
            
        }else{
            self.btnStatus.setTitle("Cancelled", for: .normal)
            
            self.isCancelled = true
            self.btnCancel.isHidden = true
            cell.btnReview.isHidden = true
            
            self.constOrderPlaced.constant = 125
            
            self.lblOrderAccept.text = "Cancelled"
            self.lblDescAccepted.text = "You have Cancelled your Order."
            
            let appColor = UIColor(named: "YellowBG")
            self.lblOrdered.backgroundColor = appColor
            self.viewAccept.backgroundColor = appColor
            
            self.lblAccept.isHidden = true
            self.viewOutDelivery.isHidden = true
            self.lblDelivery.isHidden = true
            self.viewDelivered.isHidden = true
            
            self.lblOutDelivery.isHidden = true
            self.lblDescDelivery.isHidden = true
            self.lblDelivered.isHidden = true
            self.lblDescDelivered.isHidden = true

        }
        
        let fname  = dicAddress?.fname ?? ""
        self.lblAddName.text = fname
        
        let street  = dicAddress?.street ?? ""
        let landmark  = dicAddress?.landmark ?? ""
        let city  = dicAddress?.city ?? ""
        let state  = dicAddress?.state ?? ""
        let country  = dicAddress?.country ?? ""
        let pincode  = dicAddress?.pincode ?? ""
        let add_type = dicAddress?.type ?? ""
        self.lblAddress.text = "\(street) Near \(landmark) \(city) \(state) ,\(country),\(pincode)"
        btnAddress.setTitle(add_type, for: .normal)
        
        let phone  = dicAddress?.phone ?? ""
        self.lblMobile.text = phone
        
        let total = arrOrderDetail?.total ?? ""
        self.lblAmount.text = "₹ \(total)"
      
        let grand_total = arrOrderDetail?.grandTotal ?? ""
        self.lblTotal.text = "₹ \(grand_total)"
        
        return cell
    }
    
    @objc func clickedReview(sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
        //  controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.dicProduct = self.dicProduct
        self.present(controller, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.isCancelled == true{
            return 130
        }else{
            return 175
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = self.storyboard?.instantiateViewController(identifier: "OrderDetailVC")as! OrderDetailVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - IBAction
   
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btnCancelOrder(_ sender: Any) {
        
        self.callingCancelOrderAPI()
        
    }
    
    func callingCancelOrderAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        let orderid  = arrOrderDetail?.orderId ?? ""
        let param = ["order_id" : orderid,"type" : "cancel_pro","status" : "1"]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(CENCEL_ORDER, parameters: param) { (response, error, statusCode) in
            
            
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                let status = response!.value(forKey: "status")as? Bool
                let dicData = response!.value(forKey: "data")as? NSDictionary
                let msg = response!.value(forKey: "msg")as? String

                if statusCode == 200{
                    
                   
                    if status == true{
                        self.view.makeToast(msg)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                else
                {
                    self.view.makeToast(msg)
                }
                GIFHUD.shared.dismiss()
            }
            else
            {
                let msg = response!.value(forKey: "msg")as? String

                GIFHUD.shared.dismiss()
                self.view.makeToast(msg)
            }
        }
    }
}
