//
//  RatingVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 16/10/21.
//

import UIKit
import SDWebImage
import HCSStarRatingView
import GiFHUD_Swift
import SVProgressHUD

class RatingVC: UIViewController {
    
    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var txtReview: UITextView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    
    var dicProduct : OrderListTabProduct?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let strImage = dicProduct!.img ?? ""
        let placeHolderImage = UIImage(named: "logo")
        if let img = strImage as? String{
            let image = "\(image_path_product)\(img)"

            let url = URL(string: image)
            self.imgProduct.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        }
        else{
            self.imgProduct.image = UIImage(named: "logo")
        }
        
        let strName = dicProduct!.name ?? ""
        self.lblName.text = strName

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func clickedBackground(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedOk(_ sender: Any) {
        callingRatingAPI()
    }
    
    func callingRatingAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        let dicData = appDelegate.dicLoginUserDetails

        let pid = dicProduct?.productId ?? ""
        
        let uid = dicData.id ?? ""
        let rate = self.viewRating.value ?? 0.0
        let toStr = "\(rate)"
        
        let param = ["user_id" : uid,"product_id" : pid,"rating" : toStr,"review" : txtReview.text!]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(RATE, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                let msg = response?.value(forKey: "msg")as? String

                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                
                if statusCode == 200{
                    self.view.makeToast(msg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                GIFHUD.shared.dismiss()

            }
            else
            {
                let msg = response?.value(forKey: "msg")as? String

                GIFHUD.shared.dismiss()
                self.view.makeToast(msg)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
   
}
