//
//  SelectAddressVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 16/10/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class SelectAddressVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!

    var delegate: PizzaDelegate?
    
    var arrAddressList : [AddressListModelData] = [AddressListModelData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        tblView.register(UINib(nibName: "AddressListCell", bundle: nil), forCellReuseIdentifier: "AddressListCell")
        callingAddressListAPI()


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    //MARK: - tableview delegate-datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "AddressListCell", for: indexPath) as! AddressListCell
        
        let arrAddress = self.arrAddressList[indexPath.row]
        
        let name = arrAddress.fname ?? ""
        cell.lblName.text = name
        
        let street = arrAddress.street ?? ""
        let landmark = arrAddress.landmark ?? ""
        let city = arrAddress.city ?? ""
        let state = arrAddress.state ?? ""
        let country = arrAddress.country ?? ""
        let pincode = arrAddress.pincode ?? ""
      
        cell.lblAddress.text = "\(street),Near \(landmark), \(city),\(state),\(country), \(pincode)"

        let phone = arrAddress.phone ?? ""
        cell.lblMobile.text = phone
        
        let type = arrAddress.type ?? ""
        cell.btnHome.setTitle(type, for: .normal)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appDelegate.isFromSelcted = true
        appDelegate.objSelectedAdd = self.arrAddressList[indexPath.row]
        
        delegate?.onPizzaReady(dicAddress: self.arrAddressList[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedHiode(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    
    @IBAction func clickedAddNewAddress(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "AddAddressVC")as! AddAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: - API Calling:-
        
    func callingAddressListAPI() {
        
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)

        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let form_data_id = "?user_id="
        let form_data_billing = "&billing=billing"
        let URL = "\(ADDRESS_LIST)\(form_data_id)\(id)\(form_data_billing)"
        
        let param = ["" : ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "data")as! NSArray

                    
                    if error == nil
                    {
                        if status == false{
                            for obj in arrResult {
                                let dicData = AddressListModelData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrAddressList.append(dicData)
                            }

                        }
                        else
                        {
                        }
                        self.tblView.reloadData()
                    
                }
                    GIFHUD.shared.dismiss()
            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
}
