//
//  AddressListVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 11/09/21.
//

import UIKit
import GiFHUD_Swift
import SVProgressHUD

class AddressListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAddNewAddress: UIButton!
    
    //MARK: - IBAction
    var arrOrderList : [OrderListModelResult] = [OrderListModelResult]()

    var arrAddressList : [AddressListModelData] = [AddressListModelData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        tblView.register(UINib(nibName: "AddressListCell", bundle: nil), forCellReuseIdentifier: "AddressListCell")
        
        lblCartCount.layer.cornerRadius = lblCartCount.frame.height/2
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height/2

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.callingOrderListAPI()
        callingAddressListAPI()

        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    //MARK: - tableview delegate-datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "AddressListCell", for: indexPath) as! AddressListCell
        
        let arrAddress = self.arrAddressList[indexPath.row]
        
        let name = arrAddress.fname ?? ""
        cell.lblName.text = name
        
        let street = arrAddress.street ?? ""
        let landmark = arrAddress.landmark ?? ""
        let city = arrAddress.city ?? ""
        let state = arrAddress.state ?? ""
        let country = arrAddress.country ?? ""
        let pincode = arrAddress.pincode ?? ""
      
        cell.lblAddress.text = "\(street),Near \(landmark), \(city),\(state),\(country), \(pincode)"

        let phone = arrAddress.phone ?? ""
        cell.lblMobile.text = phone
        
        let type = arrAddress.type ?? ""
        cell.btnHome.setTitle(type, for: .normal)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    //MARK: - IBAction

    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedLike(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "FavoriteVC")as! FavoriteVC
                self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedNotify(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "MyCartVC")as! MyCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            loginViewController.isShowBackButton = true
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
        
    }
    
    
    @IBAction func clickedAddNewAddress(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "AddAddressVC")as! AddAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: - API Calling:-
    
    func callingOrderListAPI() {
        
      
        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let URL = "\(ORDER_LIST)\(id)"
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    if status == true{
                        self.arrOrderList.removeAll()
                        for obj in arrResult {
                            let dicData = OrderListModelResult(fromDictionary: (obj as? NSDictionary)!)
                            self.arrOrderList.append(dicData)
                            
                           if self.arrOrderList.count > 0{
                                let arrCount = self.arrOrderList.count
                                self.lblCartCount.text = "\(arrCount)"
                            }
                        }
                        if arrResult.count == 0
                        {
                            self.lblCartCount.text = "0"
                        }
                        
                    }else{
                    }
                }

            }
            else
            {
            }
        }
    }
        
    func callingAddressListAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let dicData = appDelegate.dicLoginUserDetails

        let ID = dicData.id
        
        let id = ID ?? ""
        let form_data_id = "?user_id="
        let form_data_billing = "&billing=billing"
        let URL = "\(ADDRESS_LIST)\(form_data_id)\(id)\(form_data_billing)"
        
        let param = ["" : ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            
                if statusCode == 200{
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "data")as! NSArray

                    if error == nil
                    {
                        self.arrAddressList.removeAll()
                        
                        if status == false{
                            for obj in arrResult {
                                let dicData = AddressListModelData(fromDictionary: (obj as? NSDictionary)!)
                                self.arrAddressList.append(dicData)
                            }
                        }
                        else
                        {
                        }
                        self.tblView.reloadData()
                    
                }
                    GIFHUD.shared.dismiss()
            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
      
}
