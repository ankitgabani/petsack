

import UIKit
import DLRadioButton
import GiFHUD_Swift
import SVProgressHUD

class AddPetProfileVC: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate {
    
    //MARK: - IBOutlet

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnCreateProfile: UIButton!
    
    @IBOutlet weak var txtPetName: UITextField!
    
    @IBOutlet weak var txtBreedName: UITextField!
    
    @IBOutlet weak var txtPetWeight: UITextField!
    @IBOutlet weak var txtPetHeight: UITextField!
    @IBOutlet weak var txtPetAge: UITextField!
    @IBOutlet weak var txtHealthNote: UITextField!
    
    @IBOutlet weak var btnMale: DLRadioButton!
    @IBOutlet weak var btnFemale: DLRadioButton!
    
    @IBOutlet weak var txtAbout: UITextView!
    //MARK: - variables

    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.validation()
        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    //MARK: - Text-Field Validation
    
    func validation(){
        
        txtPetName.delegate = self
        txtBreedName.delegate = self
        txtPetWeight.delegate = self
        txtPetHeight.delegate = self
        txtPetAge.delegate = self
        txtHealthNote.delegate = self
        
        txtPetName.setLeftPaddingPoints(10)
        txtBreedName.setLeftPaddingPoints(10)
        txtPetWeight.setLeftPaddingPoints(10)
        txtPetHeight.setLeftPaddingPoints(10)
        txtPetAge.setLeftPaddingPoints(10)
        txtHealthNote.setLeftPaddingPoints(10)
        
        btnMale.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
        btnMale.isSelected = false
        btnFemale.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
        btnFemale.isSelected = false
        
    }
    
    func isValidatedLogin() -> Bool {
        if txtPetName.text == "" {
            self.view.makeToast("Pet Name not valid")
            return false
        } else if txtBreedName.text == "" {
            self.view.makeToast("Breed Name not valid")
            return false
        } else if txtPetAge.text == "" {
            self.view.makeToast("Pet Age not valid")
            return false
        }else if txtPetWeight.text == "" {
            self.view.makeToast("Pet Weight not valid")
            return false
        } else if txtPetHeight.text == "" {
            self.view.makeToast("Pet Height not valid")
            return false
        } else if txtHealthNote.text == "" {
            self.view.makeToast("Health Note not valid")
            return false
        } else if btnMale.isSelected == false && btnFemale.isSelected == false {
            self.view.makeToast("Gender not valid")
            return false
        }
        return true
    }
    
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case txtPetName:
            txtBreedName.becomeFirstResponder()
        case txtBreedName:
            txtPetWeight.becomeFirstResponder()
        case txtPetWeight:
            txtPetHeight.becomeFirstResponder()
        case txtPetHeight:
            txtPetAge.becomeFirstResponder()
        case txtPetAge:
            txtHealthNote.resignFirstResponder()
        default:
            txtHealthNote.resignFirstResponder()
        }
        return true
    }
    
    //MARK: - ImageUpload
    
//    func myImageUploadRequest(imageToUpload: UIImage, imgKey: String) {
//
//    //    AppConstant.shared.loadingView.show()
//
//        let urlString = BASE_URL + PET_PIC
//
//        let myUrl = NSURL(string: urlString)
//        let request = NSMutableURLRequest(url:myUrl! as URL);
//        request.httpMethod = "POST";
//
//        let token = AppConstant.shared.token as! String
//        let headers = ["Accept" : "application/json", "Authorization" : "Bearer \(token)"]
//        request.allHTTPHeaderFields = headers
//
//        let param = NSMutableDictionary()
//        let userId = AppConstant.shared.user?.id as! Int
//        let firstName = AppConstant.shared.user?.user_first_name as! String
//        let lastName = AppConstant.shared.user?.user_last_name as! String
//        let email = AppConstant.shared.user?.user_email as! String
//        let mobileNo = (AppConstant.shared.user?.user_mobile_no!)!
//        let country_cd = AppConstant.shared.user?.user_country_phone_code as! String
//        let dob = AppConstant.shared.user?.user_dob as! String
//        param["user_id"] = "\(userId)"
//        param["first_name"] = firstName
//        param["last_name"] = lastName
//        param["email"] = email
//        param["device_token"] = AppConstant.shared.deviceToken
//        param["device_srno"] = AppConstant.shared.deviceSerialNumber
//        param["mobile_type"] = "I"
//        param["first_name"] = firstName
//        param["dob"] = dob
//        param["mobile_no"] = "\(mobileNo)"
//        param["direct_login"] = "N"
//        param["country_cd"] = country_cd
//
//        print(param)
//
//        let boundary = generateBoundaryString()
//        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        let imageData = imageToUpload.jpegData(compressionQuality: 0.1)
//        if imageData == nil  {
//            return
//        }
//
//        request.httpBody = createBodyWithParameters(parameters: param as! [String : String], filePathKey: imgKey, imageDataKey: imageData! as NSData, boundary: boundary, imgKey: imgKey) as Data
//
//        let task = URLSession.shared.dataTask(with: request as URLRequest) {
//            data, response, error in
//
//            AppConstant.shared.loadingView.stop()
//
//            if error != nil {
//                print("error=\(error!)")
//                return
//            }
//
//            // print reponse body
//            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//            print("response data = \(responseString!)")
//
//            let httpResponse = response as? HTTPURLResponse
//
//            let response_code = httpResponse?.statusCode
//
//            DispatchQueue.main.async {
//
//
//                do{
//                    if response_code == 200 || response_code == 201 {
//
//                        let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : Any]
//                        let successMessage = dict!["message"] as! String
//                        let status = dict!["success"] as! Bool
//                        if status{
//                            let jsonDecoder = JSONDecoder()
//                            let responseModel = try jsonDecoder.decode(ProfileUpdateResponse?.self, from: data!)
//                            AppConstant.shared.user = responseModel?.user
//
//
//                        }else{
//                            AlertView.instance.showAlert(title: AppConstant.shared.AppName, message: "Failure", alertType: .oneButton)
//                        }
//                    }else if response_code == 503{
//                        AlertView.instance.showAlert(title: AppConstant.shared.AppName, message: "Failure", alertType: .oneButton)
//                    }else{
//                        AlertView.instance.showAlert(title: AppConstant.shared.AppName, message: "Failure", alertType: .oneButton)
//                    }
//                    AppConstant.shared.loadingView.stop()
//
//                }
//                catch let error {
//                    AppConstant.shared.loadingView.stop()
//                    AlertView.instance.showAlert(title: AppConstant.shared.AppName, message: error.localizedDescription, alertType: .oneButton)
//                }
//
//            }
//
//        }
//        task.resume()
//    }

    
    //MARK: - Image picker
   
//    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, imgKey: String) -> NSData {
//        let body = NSMutableData();
//        
//        if parameters != nil {
//            for (key, value) in parameters! {
//                body.appendString(string: "--\(boundary)\r\n")
//                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//                body.appendString(string: "\(value)\r\n")
//            }
//        }
//        
//        let filename = "\(imgKey).jpg"
//        let mimetype = "image/jpg"
//        
//        body.appendString(string: "--\(boundary)\r\n")
//        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
//        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
//        body.append(imageDataKey as Data)
//        body.appendString(string: "\r\n")
//        body.appendString(string: "--\(boundary)--\r\n")
//        
//        return body
//    }
//    
//    func generateBoundaryString() -> String {
//        return "Boundary-\(NSUUID().uuidString)"
//    }
//    
//    @objc func choosePicture() {
//        let alert  = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
//        alert.modalPresentationStyle = .overCurrentContext
//        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
//            self.openCamera()
//        }))
//        
//        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
//            self.openGallary()
//        }))
//        
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        
//        let popoverController = alert.popoverPresentationController
//        
//        popoverController?.permittedArrowDirections = .up
//        
//        self.present(alert, animated: true, completion: nil)
//    }
//    
//    func openCamera() {
//        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
//            showPicker()
//        }
//        else {
//            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
//    
//    func openGallary() {
//        showPicker()
//    }
//    
    //MARK: - IBAction
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedMale(_ sender: Any) {
        
        btnMale.isSelected = true
        btnMale.setImage(UIImage(named: "ic_check"), for: .normal)
        
        btnFemale.isSelected = false
        btnFemale.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
    }
    
    @IBAction func clickedFemale(_ sender: Any) {
        
        btnFemale.isSelected = true
        btnFemale.setImage(UIImage(named: "ic_check"), for: .normal)
        
        btnMale.isSelected = false
        btnMale.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
    }
    
    @IBAction func clickedProfile(_ sender: Any) {
        
        
    }
    
    @IBAction func clickedCreateProfile(_ sender: Any) {
        
        if isValidatedLogin(){
            callingPetProfileAPI()
        }
    }
    
    //MARK: - API Calling:-
    
    func callingPetProfileAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        
        let dicData = appDelegate.dicLoginUserDetails

        let email = dicData.email ?? ""
        let id = dicData.id ?? ""
        
        let type = self.btnMale.isSelected
        var text = String()
        if type == true{
            text = "Male"
        }else{
            text = "Female"
        }
       
        let param = ["user_id" : id,"id" : "","name" : txtPetName.text!,"gender": text ,"height" : txtPetHeight.text!,"weight" : txtPetWeight.text!,"age" : txtPetAge.text!,"breed" : txtBreedName.text!,"about" : txtAbout.text!,"health_note" : txtHealthNote.text!,"status" : ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(ADD_PET, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
          
            if error == nil
            {
                let status = response!.value(forKey: "status")as! Bool
                let msg = response!.value(forKey: "msg")as! String
                
                if statusCode == 200{
                    self.view.makeToast(msg)
                    
                    if status == true {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }else{
                    self.view.makeToast(msg)
                }
                GIFHUD.shared.dismiss()
            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
}


//MARK: - YPImagePicker

//extension AddPetProfileVC: YPImagePickerDelegate {
//    private func showPicker() {
//        
//        var config = YPImagePickerConfiguration()
//        config.library.mediaType = .photo
//        config.library.onlySquare = true
//        config.showsPhotoFilters = false
//        config.shouldSaveNewPicturesToAlbum = true
//        config.albumName = "RecipeSharing"
//        config.video.compression = AVAssetExportPresetMediumQuality
//        config.startOnScreen = .library
//        config.screens = [.library,.photo]
//        config.targetImageSize = YPImageSize.original
//        config.wordings.libraryTitle = "Gallery"
//        config.hidesStatusBar = false
//        config.hidesBottomBar = false
//        
//        let picker = YPImagePicker(configuration: config)
//        picker.imagePickerDelegate = self
//        picker.didFinishPicking { [unowned picker] items, _ in
//            if let photo = items.singlePhoto {
//                print(photo.fromCamera) // Image source (camera or library)
//                print(photo.image) // Final image selected by the user
//                print(photo.originalImage) // original image selected by the user, unfiltered
//                print(photo.modifiedImage) // Transformed image, can be nil
//                print(photo.exifMeta) // Print exif meta data of original image.
//                self.imgUser.image = photo.image
//                self.myImageUploadRequest(imageToUpload: photo.image, imgKey: "profile_image")
//            }
//            picker.dismiss(animated: true, completion: nil)
//        }
//        
//        isFromImagePicker = true
//        present(picker, animated: true, completion: nil)
//    }
//    
//    func noPhotos() {}
//    
//    func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
//        return true// indexPath.row != 2
//    }
//    
//}
//
