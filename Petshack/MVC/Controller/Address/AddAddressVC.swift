//
//  AddAddressVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 09/09/21.
//

import UIKit
import DLRadioButton
import DropDown
import GiFHUD_Swift
import SVProgressHUD

class AddAddressVC: UIViewController,UITextFieldDelegate {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var txtFullName: UITextField!
    
    @IBOutlet weak var txtMobileName: UITextField!
    
    @IBOutlet weak var txtHouseNo: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtLandMark: UITextField!
    @IBOutlet weak var txtPincode: UITextField!
    
    @IBOutlet weak var btnHome: DLRadioButton!
    @IBOutlet weak var btnOffice: DLRadioButton!
    
    @IBOutlet weak var btnSaveAddress: UIButton!
    
    @IBOutlet weak var viewSelectCountry: UIView!
    @IBOutlet weak var btnSelectCountry: UIButton!
    
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var viewSelectState: UIView!
    @IBOutlet weak var btnSelectState: UIButton!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var viewSelectCity: UIView!
    @IBOutlet weak var btnSelectCity: UIButton!
    @IBOutlet weak var lblCity: UILabel!
    //MARK: - variables
    
    let dropDown = DropDown()
    var arrCountryData: [getCountryListResult] = [getCountryListResult]()
    
    let dropDownState = DropDown()
    var arrStateData: [getStateListResult] = [getStateListResult]()
    
    let dropDownCity = DropDown()
    var arrCityData: [getCityListResult] = [getCityListResult]()
    
    var strCountryID = ""
    var strStateID = ""
    var strCityID = ""
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetUpData()
        GetCountryAPI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    //MARK: - Text-Field Validation
    
    func SetUpData(){
        
        txtFullName.delegate = self
        txtMobileName.delegate = self
        txtHouseNo.delegate = self
        txtArea.delegate = self
        txtLandMark.delegate = self
        txtPincode.delegate = self
        
        txtFullName.setLeftPaddingPoints(10)
        txtMobileName.setLeftPaddingPoints(10)
        txtHouseNo.setLeftPaddingPoints(10)
        txtArea.setLeftPaddingPoints(10)
        txtLandMark.setLeftPaddingPoints(10)
        txtPincode.setLeftPaddingPoints(10)
        
        btnHome.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
        btnHome.isSelected = false
        btnOffice.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
        btnOffice.isSelected = false
        
    }
    
    func isValidatedLogin() -> Bool {
        if txtFullName.text == "" {
            self.view.makeToast("Full Name not valid")
            return false
        } else if txtMobileName.text == "" {
            self.view.makeToast("Mobile Number not valid")
            return false
        } else if txtLandMark.text == "" {
            self.view.makeToast("Landmark not valid")
            return false
        }else if txtHouseNo.text == "" {
            self.view.makeToast("House Number not valid")
            return false
        } else if txtArea.text == "" {
            self.view.makeToast("Area not valid")
            return false
        } else if txtPincode.text == "" {
            self.view.makeToast("Pincode not valid")
            return false
        }else if lblCountry.text == "" {
            self.view.makeToast("country not valid")
            return false
        }else if lblState.text == "" {
            self.view.makeToast("State not valid")
            return false
        }else if lblCity.text == "" {
            self.view.makeToast("city not valid")
            return false
        }else if btnHome.isSelected == false && btnOffice.isSelected == false {
            self.view.makeToast("Address not valid")
            return false
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case txtFullName:
            txtMobileName.becomeFirstResponder()
        case txtMobileName:
            txtHouseNo.becomeFirstResponder()
        case txtHouseNo:
            txtArea.becomeFirstResponder()
        case txtArea:
            txtLandMark.becomeFirstResponder()
        case txtLandMark:
            txtPincode.resignFirstResponder()
        default:
            txtPincode.resignFirstResponder()
        }
        return true
    }
    
    
    //MARK: - setDropDown
    func setDropDownCountryCode() {
        
        let arrCountryList = NSMutableArray()
        
        for objData in arrCountryData
        {
            let name = objData.name
            arrCountryList.add(name!)
        }
        
        dropDown.dataSource = arrCountryList as! [String]
        dropDown.anchorView = viewSelectCountry
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            let fullName = item.components(separatedBy: " ")
            let objCode = fullName.last!
            
            self.strCountryID = "\(index+1)"
            
            lblCountry.text = objCode
            GetStateListAPI()
        }
        
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    func setDropDownStateCode() {
        
        let arrStateList = NSMutableArray()
        
        for objData in arrStateData
        {
            let name = objData.name
            arrStateList.add(name!)
        }
        
        dropDownState.dataSource = arrStateList as! [String]
        dropDownState.anchorView = viewSelectState
        dropDownState.direction = .bottom
        
        dropDownState.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            let fullName = item.components(separatedBy: " ")
            let objCode = fullName.last!
            
            self.strStateID = "\(index+1)"
            lblState.text = objCode
            GetCityListAPI()
        }
        
        dropDownState.dismissMode = .onTap
        dropDownState.textColor = UIColor.darkGray
        dropDownState.backgroundColor = UIColor.white
        dropDownState.selectionBackgroundColor = UIColor.clear
        dropDownState.reloadAllComponents()
    }
    
    func setDropDownCityCode() {
        
        let arrCityList = NSMutableArray()
        
        for objData in arrCityData
        {
            let name = objData.name
            arrCityList.add(name!)
        }
        
        dropDownCity.dataSource = arrCityList as! [String]
        dropDownCity.anchorView = viewSelectCity
        dropDownCity.direction = .bottom
        
        dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            let fullName = item.components(separatedBy: " ")
            let objCode = fullName.last!
            
            self.strCityID = "\(index+1)"
            lblCity.text = objCode
        }
        
        dropDownCity.dismissMode = .onTap
        dropDownCity.textColor = UIColor.darkGray
        dropDownCity.backgroundColor = UIColor.white
        dropDownCity.selectionBackgroundColor = UIColor.clear
        dropDownCity.reloadAllComponents()
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedCountry(_ sender: Any) {
        
        dropDown.show()
        
    }
    @IBAction func clickedState(_ sender: Any) {
        dropDownState.show()
    }
    
    @IBAction func clickedCity(_ sender: Any) {
        dropDownCity.show()
    }
    
    @IBAction func clickedHome(_ sender: Any) {
        
        btnHome.isSelected = true
        btnHome.setImage(UIImage(named: "ic_check"), for: .normal)
        
        btnOffice.isSelected = false
        btnOffice.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
    }
    
    @IBAction func clickedOffice(_ sender: Any) {
        
        btnOffice.isSelected = true
        btnOffice.setImage(UIImage(named: "ic_check"), for: .normal)
        
        btnHome.isSelected = false
        btnHome.setImage(UIImage(named: "ic_nonCheck"), for: .normal)
    }
    
    @IBAction func clickedSaveAddress(_ sender: Any) {
        
        if isValidatedLogin(){
            callingAddressAPI()
        }
        
    }
    
    //MARK: - API Calling:-
    
    func callingAddressAPI() {
        
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let dicData = appDelegate.dicLoginUserDetails

        let email = dicData.email ?? ""
        let id = dicData.id ?? ""
        
        let type = self.btnHome.isSelected
        var text = String()
        if type == true{
            text = "Home"
        }else{
            text = "Office"
        }
        
        let param = ["user_id" : id,"fname" : txtFullName.text!,"lname" : "","email": email ,"phone" : txtMobileName.text!,"country" : lblCountry.text!,"state" : lblState.text!,"city" : lblCity.text!,"street" : "\(txtHouseNo.text ?? "") \(txtArea.text ?? "")","landmark" : txtLandMark.text!,"pincode" : txtPincode.text!,"type" : text] as [String : Any]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(ADD_ADDRESS, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
          
            if error == nil
            {
                let status = response!.value(forKey: "status")as! Bool
                let msg = response!.value(forKey: "message")as! String
                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                
                if statusCode == 200{
                    self.view.makeToast(msg)
                    
                    if status == true {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }
                GIFHUD.shared.dismiss()
            }
            else
            {
                let msg = response!.value(forKey: "message")as! String
                GIFHUD.shared.dismiss()
                self.view.makeToast(msg)
            }
        }
    }
    
    func GetCountryAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        let param = ["" : ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_COUNTRY, parameters: param) { [self] (response, error, statusCode) in
            
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                let success = response?.value(forKey: "status") as? Bool
                let responseMsg = response?.value(forKey: "message") as? String

                if success!{
                    
                    let arrResult = (response?.value(forKey: "result") as? NSArray)!
                    
                    for obj in arrResult {
                        let dicData = getCountryListResult(fromDictionary: (obj as? NSDictionary)!)
                        self.arrCountryData.append(dicData)
                    }
                    
                    self.setDropDownCountryCode()
                }
                GIFHUD.shared.dismiss()
            }else{
                let responseMsg = response?.value(forKey: "message") as? String

                self.view.makeToast(responseMsg)
                GIFHUD.shared.dismiss()
            }
        }
    }
    
    func GetStateListAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let country_id = self.strCountryID
        let get_state = GET_STATE
        let state = "?id="
        
        let URL = "\(get_state)\(state)\(country_id)"
        let param = ["" : ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {
                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                let status = response!.value(forKey: "status")as! Bool
                
                if status{
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    for obj in arrResult {
                        let dicData = getStateListResult(fromDictionary: (obj as? NSDictionary)!)
                        self.arrStateData.append(dicData)
                    }
                    
                    self.setDropDownStateCode()
                    
                    if statusCode == 200{
                        //  self.view.makeToast(msg)
                    }
                }
                GIFHUD.shared.dismiss()

            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
    
    func GetCityListAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let country_id = self.strStateID
        let get_city = GET_CITY
        let state = "?id="
        
        let URL = "\(get_city)\(state)\(country_id)"
        let param = ["" : ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(URL, parameters: param) { (response, error, statusCode) in
            
            print("statusCode\(String(describing: statusCode))")
            print("Response\(String(describing: response))")
            
            if error == nil
            {

                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                let status = response!.value(forKey: "status")as! Bool
                
                if status{
                    let arrResult = response!.value(forKey: "result")as! NSArray
                    
                    for obj in arrResult {
                        let dicData = getCityListResult(fromDictionary: (obj as? NSDictionary)!)
                        self.arrCityData.append(dicData)
                    }
                    
                    self.setDropDownCityCode()
                    
                    if statusCode == 200{
                        //  self.view.makeToast(msg)
                    }
                }
                GIFHUD.shared.dismiss()

            }
            else
            {
                GIFHUD.shared.dismiss()
            }
        }
    }
    
}
