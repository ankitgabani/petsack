//
//  PetDetailVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 27/09/21.
//

import UIKit

class PetDetailVC: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var lblBreed: UILabel!
    
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHealthNote: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    
    var arrPetData : Result?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.lblName.text = arrPetData?.name ?? ""
        let age = arrPetData?.age ?? ""
        self.lblAge.text = "\(age) Years"
        self.lblBreed.text = arrPetData?.breed ?? ""
        let weight = arrPetData?.weight ?? ""
        self.lblWeight.text = "\(weight) Kg"
        self.lblHealthNote.text = arrPetData?.healthNote ?? ""
        self.lblAbout.text = arrPetData?.about ?? ""
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appColor = UIColor(named: "YellowBG")
        self.tabBarController?.tabBar.backgroundColor = appColor
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}
