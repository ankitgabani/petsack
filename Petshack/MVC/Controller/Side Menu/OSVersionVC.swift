//
//  OSVersionVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 30/09/21.
//

import UIKit

class OSVersionVC: UIViewController {
    @IBOutlet weak var lblVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                self.lblVersion.text = appVersion
        // Do any additional setup after loading the view.
    }

    @IBAction func clickedOK(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)        
    }
}
