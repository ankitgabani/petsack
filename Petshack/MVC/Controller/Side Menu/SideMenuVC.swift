//
//  SideMenuVC.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit
import LGSideMenuController
import GiFHUD_Swift
import SVProgressHUD

class SideMenuVC: UIViewController,UITableViewDelegate,UITableViewDataSource, UIGestureRecognizerDelegate {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var btnLogin: UIButton!
    
    //MARK: - Variables
    
    let arrSection = ["Categories","Services","Communicate"]
    var arrImages = ["ic_appversion","ic_share","ic_aboutus","ic_support"]
    var arrName = ["App Version","Share App","About Us","Support"]
    
    var arrCategories : [GetHomeDataBread] = [GetHomeDataBread]()
    var arrServices : [GetHomeDataBread] = [GetHomeDataBread]()
    var arrSubCategory : [GetHomeDataSubCategory] = [GetHomeDataSubCategory]()
    var arrContaianObjc = NSMutableArray()
    
    //MARK: - view cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let dicData = appDelegate.dicLoginUserDetails

            let email = dicData.email
            let name = dicData.name
            self.lblName.text = name
            self.lblEmail.text = email
            
            btnLogin.isHidden = true
        }
        else
        {
            btnLogin.isHidden = false
            
            self.lblName.text = ""
            self.lblEmail.text = ""
        }
        
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        
        tblView.delegate = self
        tblView.dataSource = self
        
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.callingHomeDataAPI()
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
        
        if isUserLogin == true
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC")as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
            homeViewController.isFromSide = true
            homeViewController.isShowBackButton = true
            navigation.viewControllers = [homeViewController]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        
    }
    
    //MARK: - tableview delegate-datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.arrServices.count > 0 && self.arrCategories.count > 0{
            return self.arrCategories.count + self.arrServices.count + 1
            
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0{
            
            return 85
        }
        else if section == self.arrCategories.count{
            
            return 85
        }
        else if section == self.arrServices.count + self.arrCategories.count{
            
            return 45
        }
        else{
            return 40
        }
    }
    
    
    
    //MARK: - heightForRowAt
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    //MARK: - cellForRowAt
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == self.arrServices.count + self.arrCategories.count{
            
            return 4
        }
        if arrContaianObjc.contains(section){
            
            if section == 0{
                
                let arrCategorie = arrCategories[section]
                let arrsubcategories = arrCategorie.subCategory
                
                return arrsubcategories!.count
            }
            else if section == self.arrCategories.count{
                
                let arrServices = self.arrServices[section-self.arrCategories.count]
                let arrsubcategories = arrServices.subCategory
                
                return arrsubcategories!.count
                
                
            }
            else if section == self.arrServices.count + self.arrCategories.count{
                
                return 4
            }
            else{
                if section < self.arrServices.count{
                    
                    let arrCategorie = self.arrCategories[section]
                    let arrsubcategories = arrCategorie.subCategory
                    return arrsubcategories!.count
                    
                    
                }
                else{
                    let arrServices = self.arrServices[section - self.arrCategories.count]
                    let arrsubcategories = arrServices.subCategory
                    return arrsubcategories!.count
                }
                
            }
            
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        
        cell.selectionStyle = .none
        cell.imgDropdown.isHidden = false
        
        if indexPath.section == 0{
            
            let arrCategorie = arrCategories[indexPath.section]
            let arrsubcategories = arrCategorie.subCategory
            
            cell.imgCategory.contentMode = UIView.ContentMode.scaleAspectFill
            cell.imgCategory.clipsToBounds = true
            cell.imgCategory.borderColor = .darkGray
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.height/2
            cell.imgCategory.layer.borderWidth = 0.5
            let array = arrsubcategories![indexPath.row]
            
            let name = array.name ?? ""
            cell.lblName.text = name
            
            let images = array.catPic ?? ""
            let placeHolderImage = UIImage(named: "logo")
            if let img = images as? String{
                let image = "\(image_path)\(img)"
                
                let url = URL(string: image)
                cell.imgCategory.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgCategory.image = UIImage(named: "logo")
            }
            
        }
        else if indexPath.section == self.arrCategories.count{
            
            let arrServices = self.arrServices[indexPath.section-self.arrCategories.count]
            let arrsubcategories = arrServices.subCategory
            
            
            cell.imgCategory.contentMode = UIView.ContentMode.scaleAspectFill
            cell.imgCategory.clipsToBounds = true
            cell.imgCategory.borderColor = .darkGray
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.height/2
            cell.imgCategory.layer.borderWidth = 0.5
            let array = arrsubcategories![indexPath.row]
            
            let name = array.name ?? ""
            cell.lblName.text = name
            
            let images = array.catPic ?? ""
            let placeHolderImage = UIImage(named: "logo")
            if let img = images as? String{
                let image = "\(image_path)\(img)"
                
                let url = URL(string: image)
                cell.imgCategory.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                cell.imgCategory.image = UIImage(named: "logo")
            }
        }
        else if indexPath.section == self.arrServices.count + self.arrCategories.count{
            
            cell.imgCategory.image = UIImage(named: arrImages[indexPath.row])
            cell.lblName.text = arrName[indexPath.row]
            cell.imgDropdown.isHidden = true
            cell.leftConst.constant = 17
            
        }
        else{
            if indexPath.section < self.arrServices.count{
                
                let arrCategorie = self.arrCategories[indexPath.section]
                let arrsubcategories = arrCategorie.subCategory
                
                cell.imgCategory.contentMode = UIView.ContentMode.scaleAspectFill
                cell.imgCategory.clipsToBounds = true
                cell.imgCategory.borderColor = .darkGray
                cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.height/2
                cell.imgCategory.layer.borderWidth = 0.5
                let array = arrsubcategories![indexPath.row]
                
                let name = array.name ?? ""
                cell.lblName.text = name
                
                let images = array.catPic ?? ""
                let placeHolderImage = UIImage(named: "logo")
                if let img = images as? String{
                    let image = "\(image_path)\(img)"
                    
                    let url = URL(string: image)
                    cell.imgCategory.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
                }
                else{
                    cell.imgCategory.image = UIImage(named: "logo")
                }
            }
            else{
                let arrServices = self.arrServices[indexPath.section - self.arrCategories.count]
                let arrsubcategories = arrServices.subCategory
                
                cell.imgCategory.contentMode = UIView.ContentMode.scaleAspectFill
                cell.imgCategory.clipsToBounds = true
                cell.imgCategory.borderColor = .darkGray
                cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.height/2
                cell.imgCategory.layer.borderWidth = 0.5
                let array = arrsubcategories![indexPath.row]
                
                let name = array.name ?? ""
                cell.lblName.text = name
                
                let images = array.catPic ?? ""
                let placeHolderImage = UIImage(named: "logo")
                if let img = images as? String{
                    let image = "\(image_path)\(img)"
                    
                    let url = URL(string: image)
                    cell.imgCategory.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
                }
                else{
                    cell.imgCategory.image = UIImage(named: "logo")
                }
            }
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 0{
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
            
            let arrCategorie = arrCategories[indexPath.section]
            let arrsubcategories = arrCategorie.subCategory
            let arrSCategory = arrsubcategories![indexPath.row]
            
            homeViewController.strID = arrCategorie.parentId
            homeViewController.strSubCategoryID = arrSCategory.id
            homeViewController.isFromSideMenu = true
            
//            homeViewController.strName = arrSCategory.name
//            homeViewController.arrSubCategory = arrsubcategories!
//            homeViewController.isFromSideMenu = true
            navigation.viewControllers = [homeViewController]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            
        }else if indexPath.section == self.arrCategories.count{
            
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
            let arrServices = self.arrServices[indexPath.section-self.arrCategories.count]
            let arrsubcategories = arrServices.subCategory
            let arrSCategory = arrsubcategories![indexPath.row]
            
            homeViewController.strID = arrServices.parentId
            homeViewController.strSubCategoryID = arrSCategory.id
            homeViewController.isFromSideMenu = true
            
//            homeViewController.strName = arrSCategory.name
//            homeViewController.arrSubCategory = arrsubcategories!
//            homeViewController.isFromSideMenu = true
            navigation.viewControllers = [homeViewController]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        
        } else if indexPath.section == self.arrServices.count + self.arrCategories.count{
    
                print(indexPath.row)
            if indexPath.row == 0
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "OSVersionVC") as! OSVersionVC
                //  controller.delegate = self
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalTransitionStyle = .crossDissolve
                
                self.present(controller, animated: true, completion: nil)
            }
            else if indexPath.row == 1
            {
                AppSupport.shareApp(inController: self)
            }
            else if indexPath.row == 2
            {
                guard let url = URL(string: "https://petshack.co.in/about") else { return }
                UIApplication.shared.open(url)
            }
            else
            {
                guard let url = URL(string: "https://petshack.co.in/contact") else { return }
                UIApplication.shared.open(url)
            }
            
            
//            cell.imgCategory.image = UIImage(named: arrImages[indexPath.row])
//            cell.lblName.text = arrName[indexPath.row]
            
        }else{
            
            if indexPath.section < self.arrServices.count{
                
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                
                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController
                
                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
                let arrCategorie = self.arrCategories[indexPath.section]
                let arrsubcategories = arrCategorie.subCategory
                let arrSCategory = arrsubcategories![indexPath.row]
                
                homeViewController.strID = arrCategorie.parentId
                homeViewController.strSubCategoryID = arrSCategory.id
                homeViewController.isFromSideMenu = true
                
//                homeViewController.strName = arrSCategory.name
//                homeViewController.arrSubCategory = arrsubcategories!
//                homeViewController.isFromSideMenu = true
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            }else{
                
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                
                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController
                
                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
                let arrServices = self.arrServices[indexPath.section - self.arrCategories.count]
                let arrsubcategories = arrServices.subCategory
                let arrSCategory = arrsubcategories![indexPath.row]
                
                homeViewController.strID = arrServices.parentId
                homeViewController.strSubCategoryID = arrSCategory.id
                homeViewController.isFromSideMenu = true
                
//                homeViewController.strName = arrSCategory.name
//                homeViewController.arrSubCategory = arrsubcategories!
//                homeViewController.isFromSideMenu = true
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if section == 0{
            
            let headerView = Bundle.main.loadNibNamed("HeaderView1", owner: self, options: nil)![0] as? HeaderView1
            
            headerView?.lblHeader.text = "Categories"
            
            let arrServices = self.arrCategories[section]
            let name = arrServices.name ?? ""
            
            headerView?.lblHeaderCat.text = name
            
            headerView?.imgView.contentMode = UIView.ContentMode.scaleAspectFill
            headerView!.imgView.clipsToBounds = true
            headerView!.imgView.borderColor = .darkGray
            headerView!.imgView.layer.cornerRadius = (headerView?.imgView.frame.height)!/2
            headerView!.imgView.layer.borderWidth = 0.5
            
            
            let images = arrServices.catPic ?? ""
            let placeHolderImage = UIImage(named: "logo")
            if let img = images as? String{
                let image = "\(image_path)\(img)"
                
                let url = URL(string: image)
                headerView?.imgView.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                headerView?.imgView.image = UIImage(named: "logo")
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapSection(gesture:)))
            headerView?.tag = section
            headerView?.addGestureRecognizer(tap)
            
            return headerView
        }
        else if section == self.arrCategories.count{
            
            let headerView = Bundle.main.loadNibNamed("HeaderView1", owner: self, options: nil)![0] as? HeaderView1
            
            headerView?.lblHeader.text = "Services"
            
            let arrServices = self.arrServices[section-self.arrCategories.count]
            let name = arrServices.name ?? ""
            
            headerView?.lblHeaderCat.text = name
            
            headerView?.imgView.contentMode = UIView.ContentMode.scaleAspectFill
            headerView!.imgView.clipsToBounds = true
            headerView!.imgView.borderColor = .darkGray
            headerView!.imgView.layer.cornerRadius = (headerView?.imgView.frame.height)!/2
            headerView!.imgView.layer.borderWidth = 0.5
            
            
            let images = arrServices.catPic ?? ""
            let placeHolderImage = UIImage(named: "logo")
            if let img = images as? String{
                let image = "\(image_path)\(img)"
                
                let url = URL(string: image)
                headerView?.imgView.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            }
            else{
                headerView?.imgView.image = UIImage(named: "logo")
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapSection(gesture:)))
            headerView?.tag = section
            headerView?.addGestureRecognizer(tap)
            
            return headerView
        }
        else if section == self.arrServices.count + self.arrCategories.count{
            
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.tblView.frame.size.width, height: 45))
            
            headerView.backgroundColor = UIColor.systemGray5
            
            let lbl = UILabel.init(frame: CGRect.init(x: 15, y: 0, width: headerView.frame.size.width-30, height: 45))
            lbl.text = "Communicate"
            headerView.addSubview(lbl)
            
            return headerView
        }
        else{
            let headerView = Bundle.main.loadNibNamed("HeaderView2", owner: self, options: nil)![0] as? HeaderView2
            
            if section < self.arrServices.count{
                
                let arrServices = self.arrCategories[section]
                let name = arrServices.name ?? ""
                
                headerView?.lblHeaderCat.text = name
                
                headerView?.imgView.contentMode = UIView.ContentMode.scaleAspectFill
                headerView!.imgView.clipsToBounds = true
                headerView!.imgView.borderColor = .darkGray
                headerView!.imgView.layer.cornerRadius = (headerView?.imgView.frame.height)!/2
                headerView!.imgView.layer.borderWidth = 0.5
                
                
                let images = arrServices.catPic ?? ""
                let placeHolderImage = UIImage(named: "logo")
                if let img = images as? String{
                    let image = "\(image_path)\(img)"
                    
                    let url = URL(string: image)
                    headerView?.imgView.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
                }
                else{
                    headerView?.imgView.image = UIImage(named: "logo")
                }
                
            }
            else{
                let arrServices = self.arrServices[section - self.arrCategories.count]
                let name = arrServices.name ?? ""
                
                headerView?.lblHeaderCat.text = name
                
                headerView?.imgView.contentMode = UIView.ContentMode.scaleAspectFill
                headerView!.imgView.clipsToBounds = true
                headerView!.imgView.borderColor = .darkGray
                headerView!.imgView.layer.cornerRadius = (headerView?.imgView.frame.height)!/2
                headerView!.imgView.layer.borderWidth = 0.5
                
                
                let images = arrServices.catPic ?? ""
                let placeHolderImage = UIImage(named: "logo")
                if let img = images as? String{
                    let image = "\(image_path)\(img)"
                    
                    let url = URL(string: image)
                    headerView?.imgView.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
                }
                else{
                    headerView?.imgView.image = UIImage(named: "logo")
                }
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapSection(gesture:)))
            headerView?.tag = section
            
            headerView?.addGestureRecognizer(tap)
            
            return headerView
        }
    }
    
    //MARK: - API Calling
    
    func callingHomeDataAPI() {
        
        GIFHUD.shared.setGif(named: "splash.gif")
        GIFHUD.shared.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        GIFHUD.shared.show(withOverlay: true,duration: 2.5)
        
        let param = ["" : ""]
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(HOME_DATA, parameters: param) { (response, error, statusCode) in
            
            if error == nil
            {
                print("statusCode\(String(describing: statusCode))")
                print("Response\(String(describing: response))")
                
                if statusCode == 200{
                    
                    let status = response!.value(forKey: "status")as! Bool
                    let arrResult = response!.value(forKey: "result")as! NSDictionary
                    
                    if status == true{
                        let arrCategory = arrResult.value(forKey: "category")as! NSArray
                        let arrServices = arrResult.value(forKey: "service")as! NSArray
                        
                        for obj in arrCategory {
                            let dicData = GetHomeDataBread(fromDictionary: (obj as? NSDictionary)!)
                            self.arrCategories.append(dicData)
                        }
                        
                        for obj in arrServices {
                            let dic = GetHomeDataBread(fromDictionary: (obj as? NSDictionary)!)
                            self.arrServices.append(dic)
                        }
                        self.tblView.reloadData()
                        
                    }
                }
                GIFHUD.shared.dismiss()
                
            }
            else
            {
                GIFHUD.shared.dismiss()
                self.callingHomeDataAPI()
            }
        }
    }
    
    @objc func tapSection(gesture: UITapGestureRecognizer) {
        
        let section = gesture.view!.tag
        
        if arrContaianObjc.count > 0{
            
            let openSection = arrContaianObjc.firstObject as! Int
            
            var arrsub : [GetHomeDataSubCategory] = []
            
            if openSection == 0{
                
                let arrCategorie = arrCategories[openSection]
                arrsub = arrCategorie.subCategory
                
            }
            else if openSection == self.arrCategories.count{
                
                let arrServices = self.arrServices[openSection-self.arrCategories.count]
                arrsub = arrServices.subCategory
                
                
                
            }
            else{
                if openSection < self.arrServices.count{
                    
                    let arrCategorie = self.arrCategories[openSection]
                    arrsub = arrCategorie.subCategory
                    
                    
                }
                else{
                    let arrServices = self.arrServices[openSection - self.arrCategories.count]
                    arrsub = arrServices.subCategory
                }
                
            }
            
            arrContaianObjc.remove(openSection)
            self.tblView.beginUpdates()
            for index in 0..<arrsub.count
            {
                let indexPath = IndexPath.init(row: index, section: openSection)
                self.tblView.deleteRows(at: [indexPath], with: .none)
                
            }
            self.tblView.endUpdates()
            
            
        }
        
        
        var arrsubcategories : [GetHomeDataSubCategory] = []
        
        if section == 0{
            
            let arrCategorie = arrCategories[section]
            arrsubcategories = arrCategorie.subCategory
            
        }
        else if section == self.arrCategories.count{
            
            let arrServices = self.arrServices[section-self.arrCategories.count]
            arrsubcategories = arrServices.subCategory
            
            
            
        }
        else{
            if section < self.arrServices.count{
                
                let arrCategorie = self.arrCategories[section]
                arrsubcategories = arrCategorie.subCategory
                
                
            }
            else{
                let arrServices = self.arrServices[section - self.arrCategories.count]
                arrsubcategories = arrServices.subCategory
            }
            
        }
        
        
        if arrContaianObjc.contains(section){
            
            arrContaianObjc.remove(section)
            
            self.tblView.beginUpdates()
            
            for index in 0..<arrsubcategories.count
            {
                let indexPath = IndexPath.init(row: index, section: section)
                self.tblView.deleteRows(at: [indexPath], with: .none)
                
            }
            
            self.tblView.endUpdates()
        }
        else{
            
            arrContaianObjc.add(section)
            
            self.tblView.beginUpdates()
            
            for index in 0..<arrsubcategories.count
            {
                let indexPath = IndexPath.init(row: index, section: section)
                self.tblView.insertRows(at: [indexPath], with: .none)
                
            }
            
            self.tblView.endUpdates()
            
            
        }
        
    }
    
}

struct AppSupport {
        
        static let appID = "1592483443"
        
        static func rateApp(){
            if let url = URL(string:"itms-apps://itunes.apple.com/app/\(appID)") {
                AppSupport.openURL(url)
            }
        }
        
        static func openURL(_ url: URL){
            if UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        static let shareText = "Petshack - \nShare to your friends"
        
        static func shareApp(inController controller:UIViewController){
            let textToShare = "\(AppSupport.shareText) \n itms-apps://itunes.apple.com/app/\(appID)"
            AppSupport.itemShare(inController: controller, items: textToShare)
        }
        
        static func itemShare(inController controller:UIViewController, items:Any){
            let objectsToShare = [items]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = controller.view
            activityVC.popoverPresentationController?.sourceRect = CGRect(x: 100, y: 200, width: 300, height: 300)
            controller.present(activityVC, animated: true, completion: nil)
        }
    }
