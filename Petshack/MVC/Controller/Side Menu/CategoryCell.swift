//
//  CategoryCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit

class CategoryCell: UITableViewCell {

    //MARK: - IBOutlet
    
    @IBOutlet weak var btnExpand: UIButton!
    @IBOutlet weak var imgDropdown: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var leftConst: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
