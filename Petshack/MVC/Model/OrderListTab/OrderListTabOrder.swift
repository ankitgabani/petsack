//
//	OrderListTabOrder.swift
//
//	Create by M1 on 29/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderListTabOrder : NSObject, NSCoding{

	var address : [OrderListTabAddres]!
	var cancelPro : String!
	var created : String!
	var grandTotal : String!
	var id : String!
	var orderId : String!
	var paymentMode : String!
	var paymentStatus : String!
	var product : [OrderListTabProduct]!
	var status : String!
	var total : String!
	var transactionId : String!
	var userId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = [OrderListTabAddres]()
		if let addressArray = dictionary["address"] as? [NSDictionary]{
			for dic in addressArray{
				let value = OrderListTabAddres(fromDictionary: dic)
				address.append(value)
			}
		}
		cancelPro = dictionary["cancel_pro"] as? String == nil ? "" : dictionary["cancel_pro"] as? String
		created = dictionary["created"] as? String == nil ? "" : dictionary["created"] as? String
		grandTotal = dictionary["grand_total"] as? String == nil ? "" : dictionary["grand_total"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		orderId = dictionary["order_id"] as? String == nil ? "" : dictionary["order_id"] as? String
		paymentMode = dictionary["payment_mode"] as? String == nil ? "" : dictionary["payment_mode"] as? String
		paymentStatus = dictionary["payment_status"] as? String == nil ? "" : dictionary["payment_status"] as? String
		product = [OrderListTabProduct]()
		if let productArray = dictionary["product"] as? [NSDictionary]{
			for dic in productArray{
				let value = OrderListTabProduct(fromDictionary: dic)
				product.append(value)
			}
		}
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		total = dictionary["total"] as? String == nil ? "" : dictionary["total"] as? String
		transactionId = dictionary["transaction_id"] as? String == nil ? "" : dictionary["transaction_id"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			var dictionaryElements = [NSDictionary]()
			for addressElement in address {
				dictionaryElements.append(addressElement.toDictionary())
			}
			dictionary["address"] = dictionaryElements
		}
		if cancelPro != nil{
			dictionary["cancel_pro"] = cancelPro
		}
		if created != nil{
			dictionary["created"] = created
		}
		if grandTotal != nil{
			dictionary["grand_total"] = grandTotal
		}
		if id != nil{
			dictionary["id"] = id
		}
		if orderId != nil{
			dictionary["order_id"] = orderId
		}
		if paymentMode != nil{
			dictionary["payment_mode"] = paymentMode
		}
		if paymentStatus != nil{
			dictionary["payment_status"] = paymentStatus
		}
		if product != nil{
			var dictionaryElements = [NSDictionary]()
			for productElement in product {
				dictionaryElements.append(productElement.toDictionary())
			}
			dictionary["product"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		if total != nil{
			dictionary["total"] = total
		}
		if transactionId != nil{
			dictionary["transaction_id"] = transactionId
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? [OrderListTabAddres]
         cancelPro = aDecoder.decodeObject(forKey: "cancel_pro") as? String
         created = aDecoder.decodeObject(forKey: "created") as? String
         grandTotal = aDecoder.decodeObject(forKey: "grand_total") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         orderId = aDecoder.decodeObject(forKey: "order_id") as? String
         paymentMode = aDecoder.decodeObject(forKey: "payment_mode") as? String
         paymentStatus = aDecoder.decodeObject(forKey: "payment_status") as? String
         product = aDecoder.decodeObject(forKey: "product") as? [OrderListTabProduct]
         status = aDecoder.decodeObject(forKey: "status") as? String
         total = aDecoder.decodeObject(forKey: "total") as? String
         transactionId = aDecoder.decodeObject(forKey: "transaction_id") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if cancelPro != nil{
			aCoder.encode(cancelPro, forKey: "cancel_pro")
		}
		if created != nil{
			aCoder.encode(created, forKey: "created")
		}
		if grandTotal != nil{
			aCoder.encode(grandTotal, forKey: "grand_total")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if orderId != nil{
			aCoder.encode(orderId, forKey: "order_id")
		}
		if paymentMode != nil{
			aCoder.encode(paymentMode, forKey: "payment_mode")
		}
		if paymentStatus != nil{
			aCoder.encode(paymentStatus, forKey: "payment_status")
		}
		if product != nil{
			aCoder.encode(product, forKey: "product")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}
		if transactionId != nil{
			aCoder.encode(transactionId, forKey: "transaction_id")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}