//
//	OrderListTabData.swift
//
//	Create by M1 on 29/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderListTabData : NSObject, NSCoding{

	var order : [OrderListTabOrder]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		order = [OrderListTabOrder]()
		if let orderArray = dictionary["order"] as? [NSDictionary]{
			for dic in orderArray{
				let value = OrderListTabOrder(fromDictionary: dic)
				order.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if order != nil{
			var dictionaryElements = [NSDictionary]()
			for orderElement in order {
				dictionaryElements.append(orderElement.toDictionary())
			}
			dictionary["order"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         order = aDecoder.decodeObject(forKey: "order") as? [OrderListTabOrder]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if order != nil{
			aCoder.encode(order, forKey: "order")
		}

	}

}