//
//	OrderListTabProduct.swift
//
//	Create by M1 on 29/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderListTabProduct : NSObject, NSCoding{

	var created : String!
	var discount : String!
	var id : String!
	var img : String!
	var name : String!
	var orderId : String!
	var price : String!
	var productId : String!
	var qty : String!
	var realPrice : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		created = dictionary["created"] as? String == nil ? "" : dictionary["created"] as? String
		discount = dictionary["discount"] as? String == nil ? "" : dictionary["discount"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		img = dictionary["img"] as? String == nil ? "" : dictionary["img"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		orderId = dictionary["order_id"] as? String == nil ? "" : dictionary["order_id"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
		productId = dictionary["product_id"] as? String == nil ? "" : dictionary["product_id"] as? String
		qty = dictionary["qty"] as? String == nil ? "" : dictionary["qty"] as? String
		realPrice = dictionary["real_price"] as? String == nil ? "" : dictionary["real_price"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if created != nil{
			dictionary["created"] = created
		}
		if discount != nil{
			dictionary["discount"] = discount
		}
		if id != nil{
			dictionary["id"] = id
		}
		if img != nil{
			dictionary["img"] = img
		}
		if name != nil{
			dictionary["name"] = name
		}
		if orderId != nil{
			dictionary["order_id"] = orderId
		}
		if price != nil{
			dictionary["price"] = price
		}
		if productId != nil{
			dictionary["product_id"] = productId
		}
		if qty != nil{
			dictionary["qty"] = qty
		}
		if realPrice != nil{
			dictionary["real_price"] = realPrice
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         created = aDecoder.decodeObject(forKey: "created") as? String
         discount = aDecoder.decodeObject(forKey: "discount") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         img = aDecoder.decodeObject(forKey: "img") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         orderId = aDecoder.decodeObject(forKey: "order_id") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         productId = aDecoder.decodeObject(forKey: "product_id") as? String
         qty = aDecoder.decodeObject(forKey: "qty") as? String
         realPrice = aDecoder.decodeObject(forKey: "real_price") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if created != nil{
			aCoder.encode(created, forKey: "created")
		}
		if discount != nil{
			aCoder.encode(discount, forKey: "discount")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if img != nil{
			aCoder.encode(img, forKey: "img")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if orderId != nil{
			aCoder.encode(orderId, forKey: "order_id")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productId != nil{
			aCoder.encode(productId, forKey: "product_id")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if realPrice != nil{
			aCoder.encode(realPrice, forKey: "real_price")
		}

	}

}