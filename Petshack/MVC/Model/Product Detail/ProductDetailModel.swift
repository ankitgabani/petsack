//
//	ProductDetailModel.swift
//
//	Create by M1 on 27/10/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProductDetailModel : NSObject, NSCoding{

	var option : [ProductDetailModelOption]!
	var product : [ProductDetailModelProduct]!
	var productPic : [ProductDetailModelProductPic]!
	var related : [ProductDetailModelRelated]!
	var review : [ProductDetailModelReview]!
	var status : Bool!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		option = [ProductDetailModelOption]()
		if let optionArray = dictionary["option"] as? [NSDictionary]{
			for dic in optionArray{
				let value = ProductDetailModelOption(fromDictionary: dic)
				option.append(value)
			}
		}
		product = [ProductDetailModelProduct]()
		if let productArray = dictionary["product"] as? [NSDictionary]{
			for dic in productArray{
				let value = ProductDetailModelProduct(fromDictionary: dic)
				product.append(value)
			}
		}
		productPic = [ProductDetailModelProductPic]()
		if let productPicArray = dictionary["product_pic"] as? [NSDictionary]{
			for dic in productPicArray{
				let value = ProductDetailModelProductPic(fromDictionary: dic)
				productPic.append(value)
			}
		}
		related = [ProductDetailModelRelated]()
		if let relatedArray = dictionary["related"] as? [NSDictionary]{
			for dic in relatedArray{
				let value = ProductDetailModelRelated(fromDictionary: dic)
				related.append(value)
			}
		}
		review = [ProductDetailModelReview]()
		if let reviewArray = dictionary["review"] as? [NSDictionary]{
			for dic in reviewArray{
				let value = ProductDetailModelReview(fromDictionary: dic)
				review.append(value)
			}
		}
		status = dictionary["status"] as? Bool == nil ? false : dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if option != nil{
			var dictionaryElements = [NSDictionary]()
			for optionElement in option {
				dictionaryElements.append(optionElement.toDictionary())
			}
			dictionary["option"] = dictionaryElements
		}
		if product != nil{
			var dictionaryElements = [NSDictionary]()
			for productElement in product {
				dictionaryElements.append(productElement.toDictionary())
			}
			dictionary["product"] = dictionaryElements
		}
		if productPic != nil{
			var dictionaryElements = [NSDictionary]()
			for productPicElement in productPic {
				dictionaryElements.append(productPicElement.toDictionary())
			}
			dictionary["product_pic"] = dictionaryElements
		}
		if related != nil{
			var dictionaryElements = [NSDictionary]()
			for relatedElement in related {
				dictionaryElements.append(relatedElement.toDictionary())
			}
			dictionary["related"] = dictionaryElements
		}
		if review != nil{
			var dictionaryElements = [NSDictionary]()
			for reviewElement in review {
				dictionaryElements.append(reviewElement.toDictionary())
			}
			dictionary["review"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         option = aDecoder.decodeObject(forKey: "option") as? [ProductDetailModelOption]
         product = aDecoder.decodeObject(forKey: "product") as? [ProductDetailModelProduct]
         productPic = aDecoder.decodeObject(forKey: "product_pic") as? [ProductDetailModelProductPic]
         related = aDecoder.decodeObject(forKey: "related") as? [ProductDetailModelRelated]
         review = aDecoder.decodeObject(forKey: "review") as? [ProductDetailModelReview]
         status = aDecoder.decodeObject(forKey: "status") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if option != nil{
			aCoder.encode(option, forKey: "option")
		}
		if product != nil{
			aCoder.encode(product, forKey: "product")
		}
		if productPic != nil{
			aCoder.encode(productPic, forKey: "product_pic")
		}
		if related != nil{
			aCoder.encode(related, forKey: "related")
		}
		if review != nil{
			aCoder.encode(review, forKey: "review")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}