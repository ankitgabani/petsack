//
//	ProductDetailModelReview.swift
//
//	Create by M1 on 27/10/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProductDetailModelReview : NSObject, NSCoding{

	var addon : String!
	var id : String!
	var name : String!
	var productId : String!
	var rating : String!
	var review : String!
	var userId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		productId = dictionary["product_id"] as? String == nil ? "" : dictionary["product_id"] as? String
		rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String
		review = dictionary["review"] as? String == nil ? "" : dictionary["review"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if addon != nil{
			dictionary["addon"] = addon
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if productId != nil{
			dictionary["product_id"] = productId
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if review != nil{
			dictionary["review"] = review
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         productId = aDecoder.decodeObject(forKey: "product_id") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         review = aDecoder.decodeObject(forKey: "review") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if productId != nil{
			aCoder.encode(productId, forKey: "product_id")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if review != nil{
			aCoder.encode(review, forKey: "review")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}