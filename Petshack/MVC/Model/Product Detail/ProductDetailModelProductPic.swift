//
//	ProductDetailModelProductPic.swift
//
//	Create by M1 on 27/10/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProductDetailModelProductPic : NSObject, NSCoding{

	var addon : String!
	var id : String!
	var img : String!
	var productId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		img = dictionary["img"] as? String == nil ? "" : dictionary["img"] as? String
		productId = dictionary["product_id"] as? String == nil ? "" : dictionary["product_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if addon != nil{
			dictionary["addon"] = addon
		}
		if id != nil{
			dictionary["id"] = id
		}
		if img != nil{
			dictionary["img"] = img
		}
		if productId != nil{
			dictionary["product_id"] = productId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         img = aDecoder.decodeObject(forKey: "img") as? String
         productId = aDecoder.decodeObject(forKey: "product_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if img != nil{
			aCoder.encode(img, forKey: "img")
		}
		if productId != nil{
			aCoder.encode(productId, forKey: "product_id")
		}

	}

}