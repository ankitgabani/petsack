//
//	Result.swift
//
//	Create by M1 on 22/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Result : NSObject, NSCoding{

	var about : String!
	var active : String!
	var addon : String!
	var age : String!
	var breed : String!
	var gender : String!
	var healthNote : String!
	var height : String!
	var id : String!
	var name : String!
	var profile : String!
	var status : String!
	var userId : String!
	var weight : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		about = dictionary["about"] as? String == nil ? "" : dictionary["about"] as? String
		active = dictionary["active"] as? String == nil ? "" : dictionary["active"] as? String
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		age = dictionary["age"] as? String == nil ? "" : dictionary["age"] as? String
		breed = dictionary["breed"] as? String == nil ? "" : dictionary["breed"] as? String
		gender = dictionary["gender"] as? String == nil ? "" : dictionary["gender"] as? String
		healthNote = dictionary["health_note"] as? String == nil ? "" : dictionary["health_note"] as? String
		height = dictionary["height"] as? String == nil ? "" : dictionary["height"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		profile = dictionary["profile"] as? String == nil ? "" : dictionary["profile"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
		weight = dictionary["weight"] as? String == nil ? "" : dictionary["weight"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if about != nil{
			dictionary["about"] = about
		}
		if active != nil{
			dictionary["active"] = active
		}
		if addon != nil{
			dictionary["addon"] = addon
		}
		if age != nil{
			dictionary["age"] = age
		}
		if breed != nil{
			dictionary["breed"] = breed
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if healthNote != nil{
			dictionary["health_note"] = healthNote
		}
		if height != nil{
			dictionary["height"] = height
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if profile != nil{
			dictionary["profile"] = profile
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if weight != nil{
			dictionary["weight"] = weight
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         about = aDecoder.decodeObject(forKey: "about") as? String
         active = aDecoder.decodeObject(forKey: "active") as? String
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         age = aDecoder.decodeObject(forKey: "age") as? String
         breed = aDecoder.decodeObject(forKey: "breed") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         healthNote = aDecoder.decodeObject(forKey: "health_note") as? String
         height = aDecoder.decodeObject(forKey: "height") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         profile = aDecoder.decodeObject(forKey: "profile") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if about != nil{
			aCoder.encode(about, forKey: "about")
		}
		if active != nil{
			aCoder.encode(active, forKey: "active")
		}
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if breed != nil{
			aCoder.encode(breed, forKey: "breed")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if healthNote != nil{
			aCoder.encode(healthNote, forKey: "health_note")
		}
		if height != nil{
			aCoder.encode(height, forKey: "height")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if profile != nil{
			aCoder.encode(profile, forKey: "profile")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if weight != nil{
			aCoder.encode(weight, forKey: "weight")
		}

	}

}