//
//	GetHomeDataHomeLeft.swift
//
//	Create by M1 on 21/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetHomeDataHomeLeft : NSObject, NSCoding{

	var active : String!
	var addon : String!
	var bannerId : String!
	var bannerName : String!
	var id : String!
	var img : String!
	var type : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		active = dictionary["active"] as? String == nil ? "" : dictionary["active"] as? String
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		bannerId = dictionary["banner_id"] as? String == nil ? "" : dictionary["banner_id"] as? String
		bannerName = dictionary["banner_name"] as? String == nil ? "" : dictionary["banner_name"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		img = dictionary["img"] as? String == nil ? "" : dictionary["img"] as? String
		type = dictionary["type"] as? String == nil ? "" : dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if active != nil{
			dictionary["active"] = active
		}
		if addon != nil{
			dictionary["addon"] = addon
		}
		if bannerId != nil{
			dictionary["banner_id"] = bannerId
		}
		if bannerName != nil{
			dictionary["banner_name"] = bannerName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if img != nil{
			dictionary["img"] = img
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         active = aDecoder.decodeObject(forKey: "active") as? String
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         bannerId = aDecoder.decodeObject(forKey: "banner_id") as? String
         bannerName = aDecoder.decodeObject(forKey: "banner_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         img = aDecoder.decodeObject(forKey: "img") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if active != nil{
			aCoder.encode(active, forKey: "active")
		}
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if bannerId != nil{
			aCoder.encode(bannerId, forKey: "banner_id")
		}
		if bannerName != nil{
			aCoder.encode(bannerName, forKey: "banner_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if img != nil{
			aCoder.encode(img, forKey: "img")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}