//
//	GetHomeDataBread.swift
//
//	Create by M1 on 21/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetHomeDataBread : NSObject, NSCoding{

	var active : String!
	var addon : String!
	var catPic : String!
	var catType : String!
	var id : String!
	var name : String!
	var parentId : String!
	var sort : String!
	var subCategory : [GetHomeDataSubCategory]!
	var type : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		active = dictionary["active"] as? String == nil ? "" : dictionary["active"] as? String
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		catPic = dictionary["cat_pic"] as? String == nil ? "" : dictionary["cat_pic"] as? String
		catType = dictionary["cat_type"] as? String == nil ? "" : dictionary["cat_type"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		parentId = dictionary["parent_id"] as? String == nil ? "" : dictionary["parent_id"] as? String
		sort = dictionary["sort"] as? String == nil ? "" : dictionary["sort"] as? String
		subCategory = [GetHomeDataSubCategory]()
		if let subCategoryArray = dictionary["sub_category"] as? [NSDictionary]{
			for dic in subCategoryArray{
				let value = GetHomeDataSubCategory(fromDictionary: dic)
				subCategory.append(value)
			}
		}
		type = dictionary["type"] as? String == nil ? "" : dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if active != nil{
			dictionary["active"] = active
		}
		if addon != nil{
			dictionary["addon"] = addon
		}
		if catPic != nil{
			dictionary["cat_pic"] = catPic
		}
		if catType != nil{
			dictionary["cat_type"] = catType
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if parentId != nil{
			dictionary["parent_id"] = parentId
		}
		if sort != nil{
			dictionary["sort"] = sort
		}
		if subCategory != nil{
			var dictionaryElements = [NSDictionary]()
			for subCategoryElement in subCategory {
				dictionaryElements.append(subCategoryElement.toDictionary())
			}
			dictionary["sub_category"] = dictionaryElements
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         active = aDecoder.decodeObject(forKey: "active") as? String
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         catPic = aDecoder.decodeObject(forKey: "cat_pic") as? String
         catType = aDecoder.decodeObject(forKey: "cat_type") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         parentId = aDecoder.decodeObject(forKey: "parent_id") as? String
         sort = aDecoder.decodeObject(forKey: "sort") as? String
         subCategory = aDecoder.decodeObject(forKey: "sub_category") as? [GetHomeDataSubCategory]
         type = aDecoder.decodeObject(forKey: "type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if active != nil{
			aCoder.encode(active, forKey: "active")
		}
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if catPic != nil{
			aCoder.encode(catPic, forKey: "cat_pic")
		}
		if catType != nil{
			aCoder.encode(catType, forKey: "cat_type")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if sort != nil{
			aCoder.encode(sort, forKey: "sort")
		}
		if subCategory != nil{
			aCoder.encode(subCategory, forKey: "sub_category")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}