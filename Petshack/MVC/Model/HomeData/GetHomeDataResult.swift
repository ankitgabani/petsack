//
//	GetHomeDataResult.swift
//
//	Create by M1 on 21/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GetHomeDataResult : NSObject, NSCoding{

	var bread : [GetHomeDataBread]!
	var category : [GetHomeDataBread]!
	var featuredProduct : [GetHomeDataFeaturedProduct]!
	var homeLeft : [GetHomeDataHomeLeft]!
	var homeMid : [GetHomeDataHomeLeft]!
	var homeRight : [GetHomeDataHomeLeft]!
	var homeTop : [GetHomeDataHomeLeft]!
	var latestProduct : [GetHomeDataFeaturedProduct]!
	var offerProduct : [GetHomeDataFeaturedProduct]!
	var service : [GetHomeDataBread]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		bread = [GetHomeDataBread]()
		if let breadArray = dictionary["bread"] as? [NSDictionary]{
			for dic in breadArray{
				let value = GetHomeDataBread(fromDictionary: dic)
				bread.append(value)
			}
		}
		category = [GetHomeDataBread]()
		if let categoryArray = dictionary["category"] as? [NSDictionary]{
			for dic in categoryArray{
				let value = GetHomeDataBread(fromDictionary: dic)
				category.append(value)
			}
		}
		featuredProduct = [GetHomeDataFeaturedProduct]()
		if let featuredProductArray = dictionary["featured_product"] as? [NSDictionary]{
			for dic in featuredProductArray{
				let value = GetHomeDataFeaturedProduct(fromDictionary: dic)
				featuredProduct.append(value)
			}
		}
		homeLeft = [GetHomeDataHomeLeft]()
		if let homeLeftArray = dictionary["home_left"] as? [NSDictionary]{
			for dic in homeLeftArray{
				let value = GetHomeDataHomeLeft(fromDictionary: dic)
				homeLeft.append(value)
			}
		}
		homeMid = [GetHomeDataHomeLeft]()
		if let homeMidArray = dictionary["home_mid"] as? [NSDictionary]{
			for dic in homeMidArray{
				let value = GetHomeDataHomeLeft(fromDictionary: dic)
				homeMid.append(value)
			}
		}
		homeRight = [GetHomeDataHomeLeft]()
		if let homeRightArray = dictionary["home_right"] as? [NSDictionary]{
			for dic in homeRightArray{
				let value = GetHomeDataHomeLeft(fromDictionary: dic)
				homeRight.append(value)
			}
		}
		homeTop = [GetHomeDataHomeLeft]()
		if let homeTopArray = dictionary["home_top"] as? [NSDictionary]{
			for dic in homeTopArray{
				let value = GetHomeDataHomeLeft(fromDictionary: dic)
				homeTop.append(value)
			}
		}
		latestProduct = [GetHomeDataFeaturedProduct]()
		if let latestProductArray = dictionary["latest_product"] as? [NSDictionary]{
			for dic in latestProductArray{
				let value = GetHomeDataFeaturedProduct(fromDictionary: dic)
				latestProduct.append(value)
			}
		}
		offerProduct = [GetHomeDataFeaturedProduct]()
		if let offerProductArray = dictionary["offer_product"] as? [NSDictionary]{
			for dic in offerProductArray{
				let value = GetHomeDataFeaturedProduct(fromDictionary: dic)
				offerProduct.append(value)
			}
		}
		service = [GetHomeDataBread]()
		if let serviceArray = dictionary["service"] as? [NSDictionary]{
			for dic in serviceArray{
				let value = GetHomeDataBread(fromDictionary: dic)
				service.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if bread != nil{
			var dictionaryElements = [NSDictionary]()
			for breadElement in bread {
				dictionaryElements.append(breadElement.toDictionary())
			}
			dictionary["bread"] = dictionaryElements
		}
		if category != nil{
			var dictionaryElements = [NSDictionary]()
			for categoryElement in category {
				dictionaryElements.append(categoryElement.toDictionary())
			}
			dictionary["category"] = dictionaryElements
		}
		if featuredProduct != nil{
			var dictionaryElements = [NSDictionary]()
			for featuredProductElement in featuredProduct {
				dictionaryElements.append(featuredProductElement.toDictionary())
			}
			dictionary["featured_product"] = dictionaryElements
		}
		if homeLeft != nil{
			var dictionaryElements = [NSDictionary]()
			for homeLeftElement in homeLeft {
				dictionaryElements.append(homeLeftElement.toDictionary())
			}
			dictionary["home_left"] = dictionaryElements
		}
		if homeMid != nil{
			var dictionaryElements = [NSDictionary]()
			for homeMidElement in homeMid {
				dictionaryElements.append(homeMidElement.toDictionary())
			}
			dictionary["home_mid"] = dictionaryElements
		}
		if homeRight != nil{
			var dictionaryElements = [NSDictionary]()
			for homeRightElement in homeRight {
				dictionaryElements.append(homeRightElement.toDictionary())
			}
			dictionary["home_right"] = dictionaryElements
		}
		if homeTop != nil{
			var dictionaryElements = [NSDictionary]()
			for homeTopElement in homeTop {
				dictionaryElements.append(homeTopElement.toDictionary())
			}
			dictionary["home_top"] = dictionaryElements
		}
		if latestProduct != nil{
			var dictionaryElements = [NSDictionary]()
			for latestProductElement in latestProduct {
				dictionaryElements.append(latestProductElement.toDictionary())
			}
			dictionary["latest_product"] = dictionaryElements
		}
		if offerProduct != nil{
			var dictionaryElements = [NSDictionary]()
			for offerProductElement in offerProduct {
				dictionaryElements.append(offerProductElement.toDictionary())
			}
			dictionary["offer_product"] = dictionaryElements
		}
		if service != nil{
			var dictionaryElements = [NSDictionary]()
			for serviceElement in service {
				dictionaryElements.append(serviceElement.toDictionary())
			}
			dictionary["service"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bread = aDecoder.decodeObject(forKey: "bread") as? [GetHomeDataBread]
         category = aDecoder.decodeObject(forKey: "category") as? [GetHomeDataBread]
         featuredProduct = aDecoder.decodeObject(forKey: "featured_product") as? [GetHomeDataFeaturedProduct]
         homeLeft = aDecoder.decodeObject(forKey: "home_left") as? [GetHomeDataHomeLeft]
         homeMid = aDecoder.decodeObject(forKey: "home_mid") as? [GetHomeDataHomeLeft]
         homeRight = aDecoder.decodeObject(forKey: "home_right") as? [GetHomeDataHomeLeft]
         homeTop = aDecoder.decodeObject(forKey: "home_top") as? [GetHomeDataHomeLeft]
         latestProduct = aDecoder.decodeObject(forKey: "latest_product") as? [GetHomeDataFeaturedProduct]
         offerProduct = aDecoder.decodeObject(forKey: "offer_product") as? [GetHomeDataFeaturedProduct]
         service = aDecoder.decodeObject(forKey: "service") as? [GetHomeDataBread]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if bread != nil{
			aCoder.encode(bread, forKey: "bread")
		}
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if featuredProduct != nil{
			aCoder.encode(featuredProduct, forKey: "featured_product")
		}
		if homeLeft != nil{
			aCoder.encode(homeLeft, forKey: "home_left")
		}
		if homeMid != nil{
			aCoder.encode(homeMid, forKey: "home_mid")
		}
		if homeRight != nil{
			aCoder.encode(homeRight, forKey: "home_right")
		}
		if homeTop != nil{
			aCoder.encode(homeTop, forKey: "home_top")
		}
		if latestProduct != nil{
			aCoder.encode(latestProduct, forKey: "latest_product")
		}
		if offerProduct != nil{
			aCoder.encode(offerProduct, forKey: "offer_product")
		}
		if service != nil{
			aCoder.encode(service, forKey: "service")
		}

	}

}