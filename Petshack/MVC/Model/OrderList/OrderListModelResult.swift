//
//	OrderListModelResult.swift
//
//	Create by M1 on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderListModelResult : NSObject, NSCoding{

	var discount : String!
	var id : String!
	var img : String!
	var name : String!
	var option : String!
	var optionValue : String!
	var price : String!
	var qty : Int!
	var realPrice : String!
	var rowid : String!
	var subtotal : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		discount = dictionary["discount"] as? String == nil ? "" : dictionary["discount"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		img = dictionary["img"] as? String == nil ? "" : dictionary["img"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		option = dictionary["option"] as? String == nil ? "" : dictionary["option"] as? String
		optionValue = dictionary["optionValue"] as? String == nil ? "" : dictionary["optionValue"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
		qty = dictionary["qty"] as? Int == nil ? 0 : dictionary["qty"] as? Int
		realPrice = dictionary["real_price"] as? String == nil ? "" : dictionary["real_price"] as? String
		rowid = dictionary["rowid"] as? String == nil ? "" : dictionary["rowid"] as? String
		subtotal = dictionary["subtotal"] as? Int == nil ? 0 : dictionary["subtotal"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if discount != nil{
			dictionary["discount"] = discount
		}
		if id != nil{
			dictionary["id"] = id
		}
		if img != nil{
			dictionary["img"] = img
		}
		if name != nil{
			dictionary["name"] = name
		}
		if option != nil{
			dictionary["option"] = option
		}
		if optionValue != nil{
			dictionary["optionValue"] = optionValue
		}
		if price != nil{
			dictionary["price"] = price
		}
		if qty != nil{
			dictionary["qty"] = qty
		}
		if realPrice != nil{
			dictionary["real_price"] = realPrice
		}
		if rowid != nil{
			dictionary["rowid"] = rowid
		}
		if subtotal != nil{
			dictionary["subtotal"] = subtotal
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         discount = aDecoder.decodeObject(forKey: "discount") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         img = aDecoder.decodeObject(forKey: "img") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         option = aDecoder.decodeObject(forKey: "option") as? String
         optionValue = aDecoder.decodeObject(forKey: "optionValue") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         qty = aDecoder.decodeObject(forKey: "qty") as? Int
         realPrice = aDecoder.decodeObject(forKey: "real_price") as? String
         rowid = aDecoder.decodeObject(forKey: "rowid") as? String
         subtotal = aDecoder.decodeObject(forKey: "subtotal") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if discount != nil{
			aCoder.encode(discount, forKey: "discount")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if img != nil{
			aCoder.encode(img, forKey: "img")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if option != nil{
			aCoder.encode(option, forKey: "option")
		}
		if optionValue != nil{
			aCoder.encode(optionValue, forKey: "optionValue")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if realPrice != nil{
			aCoder.encode(realPrice, forKey: "real_price")
		}
		if rowid != nil{
			aCoder.encode(rowid, forKey: "rowid")
		}
		if subtotal != nil{
			aCoder.encode(subtotal, forKey: "subtotal")
		}

	}

}
