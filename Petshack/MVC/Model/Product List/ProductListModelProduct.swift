//
//	ProductListModelProduct.swift
//
//	Create by M1 on 30/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProductListModelProduct : NSObject, NSCoding{

	var active : String!
	var addon : String!
	var barcode : String!
	var catId : String!
	var descriptionField : String!
	var discountPrice : String!
	var endDate : String!
	var featured : String!
	var id : String!
	var img : String!
	var img2 : String!
	var latest : String!
	var minCatId : String!
	var miniQaunty : String!
	var offer : String!
	var option : [ProductListModelOption]!
	var parentId : String!
	var price : String!
	var prodcutQuantity : String!
	var productName : String!
	var returnPolicy : String!
	var startDate : String!
	var subCatId : String!
	var tax : String!
	var uom : String!
	var updated : String!
	var vendorId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		active = dictionary["active"] as? String == nil ? "" : dictionary["active"] as? String
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		barcode = dictionary["barcode"] as? String == nil ? "" : dictionary["barcode"] as? String
		catId = dictionary["cat_id"] as? String == nil ? "" : dictionary["cat_id"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		discountPrice = dictionary["discount_price"] as? String == nil ? "" : dictionary["discount_price"] as? String
		endDate = dictionary["end_date"] as? String == nil ? "" : dictionary["end_date"] as? String
		featured = dictionary["featured"] as? String == nil ? "" : dictionary["featured"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		img = dictionary["img"] as? String == nil ? "" : dictionary["img"] as? String
		img2 = dictionary["img2"] as? String == nil ? "" : dictionary["img2"] as? String
		latest = dictionary["latest"] as? String == nil ? "" : dictionary["latest"] as? String
		minCatId = dictionary["min_cat_id"] as? String == nil ? "" : dictionary["min_cat_id"] as? String
		miniQaunty = dictionary["mini_qaunty"] as? String == nil ? "" : dictionary["mini_qaunty"] as? String
		offer = dictionary["offer"] as? String == nil ? "" : dictionary["offer"] as? String
		option = [ProductListModelOption]()
		if let optionArray = dictionary["option"] as? [NSDictionary]{
			for dic in optionArray{
				let value = ProductListModelOption(fromDictionary: dic)
				option.append(value)
			}
		}
		parentId = dictionary["parent_id"] as? String == nil ? "" : dictionary["parent_id"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
		prodcutQuantity = dictionary["prodcut_quantity"] as? String == nil ? "" : dictionary["prodcut_quantity"] as? String
		productName = dictionary["product_name"] as? String == nil ? "" : dictionary["product_name"] as? String
		returnPolicy = dictionary["return_policy"] as? String == nil ? "" : dictionary["return_policy"] as? String
		startDate = dictionary["start_date"] as? String == nil ? "" : dictionary["start_date"] as? String
		subCatId = dictionary["sub_cat_id"] as? String == nil ? "" : dictionary["sub_cat_id"] as? String
		tax = dictionary["tax"] as? String == nil ? "" : dictionary["tax"] as? String
		uom = dictionary["uom"] as? String == nil ? "" : dictionary["uom"] as? String
		updated = dictionary["updated"] as? String == nil ? "" : dictionary["updated"] as? String
		vendorId = dictionary["vendor_id"] as? String == nil ? "" : dictionary["vendor_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if active != nil{
			dictionary["active"] = active
		}
		if addon != nil{
			dictionary["addon"] = addon
		}
		if barcode != nil{
			dictionary["barcode"] = barcode
		}
		if catId != nil{
			dictionary["cat_id"] = catId
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if discountPrice != nil{
			dictionary["discount_price"] = discountPrice
		}
		if endDate != nil{
			dictionary["end_date"] = endDate
		}
		if featured != nil{
			dictionary["featured"] = featured
		}
		if id != nil{
			dictionary["id"] = id
		}
		if img != nil{
			dictionary["img"] = img
		}
		if img2 != nil{
			dictionary["img2"] = img2
		}
		if latest != nil{
			dictionary["latest"] = latest
		}
		if minCatId != nil{
			dictionary["min_cat_id"] = minCatId
		}
		if miniQaunty != nil{
			dictionary["mini_qaunty"] = miniQaunty
		}
		if offer != nil{
			dictionary["offer"] = offer
		}
		if option != nil{
			var dictionaryElements = [NSDictionary]()
			for optionElement in option {
				dictionaryElements.append(optionElement.toDictionary())
			}
			dictionary["option"] = dictionaryElements
		}
		if parentId != nil{
			dictionary["parent_id"] = parentId
		}
		if price != nil{
			dictionary["price"] = price
		}
		if prodcutQuantity != nil{
			dictionary["prodcut_quantity"] = prodcutQuantity
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
		if returnPolicy != nil{
			dictionary["return_policy"] = returnPolicy
		}
		if startDate != nil{
			dictionary["start_date"] = startDate
		}
		if subCatId != nil{
			dictionary["sub_cat_id"] = subCatId
		}
		if tax != nil{
			dictionary["tax"] = tax
		}
		if uom != nil{
			dictionary["uom"] = uom
		}
		if updated != nil{
			dictionary["updated"] = updated
		}
		if vendorId != nil{
			dictionary["vendor_id"] = vendorId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         active = aDecoder.decodeObject(forKey: "active") as? String
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         barcode = aDecoder.decodeObject(forKey: "barcode") as? String
         catId = aDecoder.decodeObject(forKey: "cat_id") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         discountPrice = aDecoder.decodeObject(forKey: "discount_price") as? String
         endDate = aDecoder.decodeObject(forKey: "end_date") as? String
         featured = aDecoder.decodeObject(forKey: "featured") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         img = aDecoder.decodeObject(forKey: "img") as? String
         img2 = aDecoder.decodeObject(forKey: "img2") as? String
         latest = aDecoder.decodeObject(forKey: "latest") as? String
         minCatId = aDecoder.decodeObject(forKey: "min_cat_id") as? String
         miniQaunty = aDecoder.decodeObject(forKey: "mini_qaunty") as? String
         offer = aDecoder.decodeObject(forKey: "offer") as? String
         option = aDecoder.decodeObject(forKey: "option") as? [ProductListModelOption]
         parentId = aDecoder.decodeObject(forKey: "parent_id") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         prodcutQuantity = aDecoder.decodeObject(forKey: "prodcut_quantity") as? String
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
         returnPolicy = aDecoder.decodeObject(forKey: "return_policy") as? String
         startDate = aDecoder.decodeObject(forKey: "start_date") as? String
         subCatId = aDecoder.decodeObject(forKey: "sub_cat_id") as? String
         tax = aDecoder.decodeObject(forKey: "tax") as? String
         uom = aDecoder.decodeObject(forKey: "uom") as? String
         updated = aDecoder.decodeObject(forKey: "updated") as? String
         vendorId = aDecoder.decodeObject(forKey: "vendor_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if active != nil{
			aCoder.encode(active, forKey: "active")
		}
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if barcode != nil{
			aCoder.encode(barcode, forKey: "barcode")
		}
		if catId != nil{
			aCoder.encode(catId, forKey: "cat_id")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if discountPrice != nil{
			aCoder.encode(discountPrice, forKey: "discount_price")
		}
		if endDate != nil{
			aCoder.encode(endDate, forKey: "end_date")
		}
		if featured != nil{
			aCoder.encode(featured, forKey: "featured")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if img != nil{
			aCoder.encode(img, forKey: "img")
		}
		if img2 != nil{
			aCoder.encode(img2, forKey: "img2")
		}
		if latest != nil{
			aCoder.encode(latest, forKey: "latest")
		}
		if minCatId != nil{
			aCoder.encode(minCatId, forKey: "min_cat_id")
		}
		if miniQaunty != nil{
			aCoder.encode(miniQaunty, forKey: "mini_qaunty")
		}
		if offer != nil{
			aCoder.encode(offer, forKey: "offer")
		}
		if option != nil{
			aCoder.encode(option, forKey: "option")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if prodcutQuantity != nil{
			aCoder.encode(prodcutQuantity, forKey: "prodcut_quantity")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}
		if returnPolicy != nil{
			aCoder.encode(returnPolicy, forKey: "return_policy")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "start_date")
		}
		if subCatId != nil{
			aCoder.encode(subCatId, forKey: "sub_cat_id")
		}
		if tax != nil{
			aCoder.encode(tax, forKey: "tax")
		}
		if uom != nil{
			aCoder.encode(uom, forKey: "uom")
		}
		if updated != nil{
			aCoder.encode(updated, forKey: "updated")
		}
		if vendorId != nil{
			aCoder.encode(vendorId, forKey: "vendor_id")
		}

	}

}