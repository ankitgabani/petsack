//
//	Option.swift
//
//	Create by M1 on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Option : NSObject, NSCoding{

	var addon : String!
	var id : String!
	var productId : String!
	var title : String!
	var value : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		productId = dictionary["product_id"] as? String == nil ? "" : dictionary["product_id"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
		value = dictionary["value"] as? String == nil ? "" : dictionary["value"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if addon != nil{
			dictionary["addon"] = addon
		}
		if id != nil{
			dictionary["id"] = id
		}
		if productId != nil{
			dictionary["product_id"] = productId
		}
		if title != nil{
			dictionary["title"] = title
		}
		if value != nil{
			dictionary["value"] = value
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         productId = aDecoder.decodeObject(forKey: "product_id") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         value = aDecoder.decodeObject(forKey: "value") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if productId != nil{
			aCoder.encode(productId, forKey: "product_id")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}