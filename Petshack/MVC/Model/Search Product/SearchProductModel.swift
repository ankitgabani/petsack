//
//	SearchProductModel.swift
//
//	Create by M1 on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SearchProductModel : NSObject, NSCoding{

	var product : [Product]!
	var status : Bool!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		product = [Product]()
		if let productArray = dictionary["product"] as? [NSDictionary]{
			for dic in productArray{
				let value = Product(fromDictionary: dic)
				product.append(value)
			}
		}
		status = dictionary["status"] as? Bool == nil ? false : dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if product != nil{
			var dictionaryElements = [NSDictionary]()
			for productElement in product {
				dictionaryElements.append(productElement.toDictionary())
			}
			dictionary["product"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         product = aDecoder.decodeObject(forKey: "product") as? [Product]
         status = aDecoder.decodeObject(forKey: "status") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if product != nil{
			aCoder.encode(product, forKey: "product")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}