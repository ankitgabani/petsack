//
//	AddressListModelData.swift
//
//	Create by M1 on 23/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AddressListModelData : NSObject, NSCoding{

	var addressId : String!
	var city : String!
	var country : String!
	var created : String!
	var defaultAddress : String!
	var email : String!
	var fname : String!
	var id : String!
	var landmark : String!
	var lname : String!
	var phone : String!
	var pincode : String!
	var state : String!
	var street : String!
	var type : String!
	var userId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		addressId = dictionary["address_id"] as? String == nil ? "" : dictionary["address_id"] as? String
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		created = dictionary["created"] as? String == nil ? "" : dictionary["created"] as? String
		defaultAddress = dictionary["default_address"] as? String == nil ? "" : dictionary["default_address"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		fname = dictionary["fname"] as? String == nil ? "" : dictionary["fname"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		landmark = dictionary["landmark"] as? String == nil ? "" : dictionary["landmark"] as? String
		lname = dictionary["lname"] as? String == nil ? "" : dictionary["lname"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		street = dictionary["street"] as? String == nil ? "" : dictionary["street"] as? String
		type = dictionary["type"] as? String == nil ? "" : dictionary["type"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if addressId != nil{
			dictionary["address_id"] = addressId
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if created != nil{
			dictionary["created"] = created
		}
		if defaultAddress != nil{
			dictionary["default_address"] = defaultAddress
		}
		if email != nil{
			dictionary["email"] = email
		}
		if fname != nil{
			dictionary["fname"] = fname
		}
		if id != nil{
			dictionary["id"] = id
		}
		if landmark != nil{
			dictionary["landmark"] = landmark
		}
		if lname != nil{
			dictionary["lname"] = lname
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if state != nil{
			dictionary["state"] = state
		}
		if street != nil{
			dictionary["street"] = street
		}
		if type != nil{
			dictionary["type"] = type
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addressId = aDecoder.decodeObject(forKey: "address_id") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         created = aDecoder.decodeObject(forKey: "created") as? String
         defaultAddress = aDecoder.decodeObject(forKey: "default_address") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         fname = aDecoder.decodeObject(forKey: "fname") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         landmark = aDecoder.decodeObject(forKey: "landmark") as? String
         lname = aDecoder.decodeObject(forKey: "lname") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         street = aDecoder.decodeObject(forKey: "street") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if addressId != nil{
			aCoder.encode(addressId, forKey: "address_id")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if created != nil{
			aCoder.encode(created, forKey: "created")
		}
		if defaultAddress != nil{
			aCoder.encode(defaultAddress, forKey: "default_address")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if fname != nil{
			aCoder.encode(fname, forKey: "fname")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if landmark != nil{
			aCoder.encode(landmark, forKey: "landmark")
		}
		if lname != nil{
			aCoder.encode(lname, forKey: "lname")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if street != nil{
			aCoder.encode(street, forKey: "street")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}