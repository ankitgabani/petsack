//
//	RegisterDataResult.swift
//
//	Create by M1 on 17/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RegisterDataResult : NSObject, NSCoding{

	var addon : String!
	var deviceId : String!
	var deviceType : String!
	var email : String!
	var id : String!
	var name : String!
	var password : String!
	var phone : String!
	var profile : String!
	var status : String!
	var token : String!
	var updated : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		addon = dictionary["addon"] as? String == nil ? "" : dictionary["addon"] as? String
		deviceId = dictionary["device_id"] as? String == nil ? "" : dictionary["device_id"] as? String
		deviceType = dictionary["device_type"] as? String == nil ? "" : dictionary["device_type"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		password = dictionary["password"] as? String == nil ? "" : dictionary["password"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		profile = dictionary["profile"] as? String == nil ? "" : dictionary["profile"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		token = dictionary["token"] as? String == nil ? "" : dictionary["token"] as? String
		updated = dictionary["updated"] as? String == nil ? "" : dictionary["updated"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if addon != nil{
			dictionary["addon"] = addon
		}
		if deviceId != nil{
			dictionary["device_id"] = deviceId
		}
		if deviceType != nil{
			dictionary["device_type"] = deviceType
		}
		if email != nil{
			dictionary["email"] = email
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if password != nil{
			dictionary["password"] = password
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if profile != nil{
			dictionary["profile"] = profile
		}
		if status != nil{
			dictionary["status"] = status
		}
		if token != nil{
			dictionary["token"] = token
		}
		if updated != nil{
			dictionary["updated"] = updated
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addon = aDecoder.decodeObject(forKey: "addon") as? String
         deviceId = aDecoder.decodeObject(forKey: "device_id") as? String
         deviceType = aDecoder.decodeObject(forKey: "device_type") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         profile = aDecoder.decodeObject(forKey: "profile") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         token = aDecoder.decodeObject(forKey: "token") as? String
         updated = aDecoder.decodeObject(forKey: "updated") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if addon != nil{
			aCoder.encode(addon, forKey: "addon")
		}
		if deviceId != nil{
			aCoder.encode(deviceId, forKey: "device_id")
		}
		if deviceType != nil{
			aCoder.encode(deviceType, forKey: "device_type")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if profile != nil{
			aCoder.encode(profile, forKey: "profile")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}
		if updated != nil{
			aCoder.encode(updated, forKey: "updated")
		}

	}

}