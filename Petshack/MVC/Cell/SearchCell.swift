//
//  SearchCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//

import UIKit

class SearchCell: UICollectionViewCell {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTypes: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    
    @IBOutlet weak var btnWish: UIButton!
    @IBOutlet weak var btnClicked: UIButton!
    
}
