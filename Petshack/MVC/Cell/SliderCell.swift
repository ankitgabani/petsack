//
//  SliderCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 10/09/21.
//

import UIKit

class SliderCell: UICollectionViewCell {
    
    //MARK: - IBOutlet

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgSlider: UIImageView!
    
}
