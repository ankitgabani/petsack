//
//  AddressDataCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 22/09/21.
//

import UIKit

class AddressDataCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblAddressType: UILabel!
    
}
