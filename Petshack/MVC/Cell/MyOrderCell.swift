//
//  MyOrderCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 13/09/21.
//

import UIKit

class MyOrderCell: UITableViewCell {
    
    //MARK: - IBOutlet

    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMode: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnPlaced: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
