//
//  PetProfileCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 09/09/21.
//

import UIKit

class PetProfileCell: UICollectionViewCell {
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgPetProfile: UIImageView!
    @IBOutlet weak var lblPetName: UILabel!
    @IBOutlet weak var lblPetHealth: UILabel!
    @IBOutlet weak var lblAboutPet: UILabel!
    
}
