//
//  AnimalCategoryCell.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 10/09/21.
//

import UIKit

class AnimalCategoryCell: UICollectionViewCell {
    
    //MARK: - IBOutlet

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgCategory: UIImageView!
}
