//
//  AppDelegate.swift
//  Petshack
//
//  Created by Rutvik Moradiya on 08/09/21.
//
//com.Petshack
import UIKit
import LGSideMenuController
import IQKeyboardManager
import UserNotifications
import FirebaseMessaging
import FirebaseAuth
import Firebase
import UserNotifications
import FirebaseCrashlytics
import FirebaseCore
import GoogleSignIn
import FBSDKCoreKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate, MessagingDelegate{

    var window: UIWindow?
    var strDeviceToken : String = ""
    var arrUserNotificationData = NSMutableArray()
    
    var dicLoginUserDetails = RegisterDataResult()
    
    var objSelectedAdd: AddressListModelData?
    var isFromSelcted = false
    var sessionID: String!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared().isEnabled = true
        
        self.dicLoginUserDetails = getCurrentUserData()
        
        GIDSignIn.sharedInstance().clientID = clientID

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    
        FirebaseApp.configure()
                
        let isUserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool

        if isUserLogin == true
        {
            setUpSideMenu()
        }
        else
        {
            SetupLoginPage()
        }
        
        setupPush()
        ConnectToFCM()
        
        // Override point for customization after application launch.
        return true
    }
    
    func successSessionID(sessionID: String) {
        print("Success SessionID...")
        print("SessionID: \(sessionID)")
        
        self.sessionID = sessionID  // Receive in a variable the unique SessionID generated
            
    }
    
    func failSessionID(error: NSError) {
        print("Fail SessionID...")
        print("\(error.code) - \(error.localizedDescription)")
    }
    
    func ConnectToFCM() {
        
        Messaging.messaging().delegate = self
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = token {
                print("Remote instance ID token: \(result)")
                self.strDeviceToken = result
                UserDefaults.standard.set(self.strDeviceToken, forKey: "FirebaseToken")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        print(userInfo)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo
        
        print(userInfo)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        
    }
    
    func setupPush()
    {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Handle user allowing / declining notification permission. Example:
            if (granted) {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            } else {
                print("User declined notification permissions")
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            // UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        if #available(iOS 10.0, *) {
            
            let center = UNUserNotificationCenter.current()
            center.delegate  = self
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                if (granted)
                {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                }
            }
        }
        else{
            
            let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
            
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    //MARK:-
    
    func SetupLoginPage(){
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "SentOtpVC") as! SentOtpVC
        loginViewController.isShowBackButton = false
        let logoutScreen = UINavigationController(rootViewController: loginViewController)
        self.window?.rootViewController = logoutScreen
        self.window?.makeKeyAndVisible()
        
    }
   
    //MARK:- set sidemenu
    
    func setUpSideMenu()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let home: TabBarVC = mainStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        
        let homeNavigation = UINavigationController(rootViewController: home)
        
        let sideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        
        let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: sideMenuVC, rightViewController: nil)
        
        controller.leftViewWidth = 280
        
        self.window?.rootViewController = controller
        self.window?.makeKeyAndVisible()
    }
    
    func saveCurrentUserData(dic: RegisterDataResult)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "CURRENT_USER_DATA")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> RegisterDataResult
    {
        if let data = UserDefaults.standard.object(forKey: "CURRENT_USER_DATA"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! RegisterDataResult
        }
        return RegisterDataResult()
    }
    
}

