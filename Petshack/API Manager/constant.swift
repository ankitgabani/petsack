//
//  constant_APi.swift
//  Parq
//
//  Created by Sahil Moradiya on 05/07/21.
//

import Foundation
import UIKit


//let BASE_URL = "https://petshack.co.in/work/"

let BASE_URL = "https://petshack.co.in/"

let NEW_BASE_URL = "https://petshack.co.in/new/"
let image_path = "https://petshack.co.in/assets/category/"
let image_path_product = "https://petshack.co.in/assets/product/"
let image_path_banner = "https://petshack.co.in/assets/banner/"

let GoogleAPIKey = "AIzaSyCu143LAf5Sko0uVh_q-1iJEABanOHA61I"

let clientID = "612934593832-veoupjfghr3e8pts6n34006fcted28od.apps.googleusercontent.com"


let SEND_OTP = "api/user/otp"
let VERIFY_OTP = "api/user/otpVerify"

let USER_DATA = "api/user/userData"
let USER_CREATE = "api/user/saveData"
let Login = "api/user/Login"
let CATEGORY = "api/category"
let WISH_ADD = "api/user/wishAdd"
let ADD_PET = "api/user/savePet"
let PET_PIC = "api/user/petpic"
let GET_PET = "api/user/petprofile"
let GET_COUNTRY = "api/order/country"
let GET_STATE = "/api/order/state"
let GET_CITY = "api/order/city"
let ADD_ADDRESS = "api/user/addresSave"
let ADDRESS_LIST = "api/order/addressList"
let HOME_DATA = "api/category/categoryData"
let PRODUCTS = "api/product/products"
let PRODUCT_DETAIL = "api/product/productDetails"
let SEARCH_PRODUCT = "api/product/products"
let ORDER_LIST = "api/order/cart?user_id="
let UPDATE_CART = "api/order/updateCart"
let REMOVE_CART = "api/order/removeCart"
let ORDER_LIST_TAB = "api/order/orderList"
let ADD_CART = "api/order/addtocart"
let CENCEL_ORDER = "api/order/orderStatus"
let RATE = "api/product/review"
let UPLOAD_IMAGE = "api/user/petpic"
let WISH_LIST = "api/user/wishList"
let PLACE_ORDER = "api/order/placeorder"


let PROFILE = "profileUpdate"
let TERMS_CONDITION = "productSecureApi"
let UV_INSERT_UPDATE = "UVInsertUpdate"
let UV_DETAILS = "UVDetails"
let UV_DEFAULT = "UVDefault"
let UV_DELETE = "UVDelete"
let VALET_HISTORY = "UserValetHistory"
let VALET_CLEAR_HISTORY = "UserClearHistory"
let TICKET_DELETE = "ticketDelete"
let ALL_TICKET_STATUS = "AllTicketStatus"
let TICKET_INSERT = "VTicketInsert"

let appDelegate = UIApplication.shared.delegate as! AppDelegate
